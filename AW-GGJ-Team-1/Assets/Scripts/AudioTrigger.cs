﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioTrigger : MonoBehaviour
{

    public AudioSource[] audioToPlay;

    static int audioCount = 0;
    
    private bool audioTriggered = false;

    // Update is called once per frame
    private void OnTriggerEnter(Collider other) 
    {
        if (!audioTriggered && other.gameObject.tag == "Player")    
        {
            if (audioCount < audioToPlay.Length)
            {
                audioToPlay[audioCount].Play();
            }
            audioCount++;
            audioTriggered = true;
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (audioCount >= 5)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }

    }
}
