﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryBox : MonoBehaviour
{
    private BoxCollider mbCollider;
    public GameObject lid;
    private float rotateSpeed = 200;
    public bool boxOpen = false;

    public bool objectClicked = false;
    public GameObject [] miniRooms;
    public GameObject [] miniRoomsStruct;
    public int miniObjectsInRoom;
    public GameObject [] spawnedObjects;
    public GameObject objectToSpawn;
    public int currentRoom = 0;
    private int nRooms = 5;
    public string clickedObjectRoom = "";

    // Start is called before the first frame update
    void Start()
    {
        mbCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (boxOpen && (lid.transform.rotation.z > -0.9)){
            rotateLid(-rotateSpeed);
            }
        else if (!boxOpen && lid.transform.rotation.z < 0){
            rotateLid(rotateSpeed);
            if (objectClicked)
            {
                mbCollider.enabled = true;
                for (int i = 0; i < 5; i++)
                {
                    spawnedObjects[i].SetActive(false);
                }
                objectClicked = false;
                Debug.Log("CLICKED OBJECT FROM: " + clickedObjectRoom);
                if(clickedObjectRoom == ("Room"+(currentRoom-1)))
                {
                    Debug.Log("CORRECT!");
                }
                else
                {
                    Debug.Log("WRONG!");
                }
            }
            }
    }

    void rotateLid(float rotateSpeed)
    {
        lid.transform.Rotate(Vector3.forward, rotateSpeed * Time.deltaTime);
    }

    private void OnMouseDown() {
        Debug.Log("Boxed clicked");
        if (!boxOpen)
        {
            boxOpen = true;
            if (currentRoom > 0)
            {
                miniRooms[currentRoom-1].SetActive(false);
            }
            else
            {
                miniRooms[nRooms-1].SetActive(false);
            }
            miniRooms[currentRoom].SetActive(true);
            mbCollider.enabled = false;

            Debug.Log("Room " + currentRoom);
            for (int i = 0; i < 5; i++)
            {
                miniObjectsInRoom = miniRoomsStruct[i].transform.childCount;
                Debug.Log(miniObjectsInRoom + " objects available from room " + i);
                spawnedObjects[i] = miniRoomsStruct[i].transform.GetChild(Random.Range(0,miniObjectsInRoom)).gameObject;
                spawnedObjects[i].SetActive(true);
                Debug.Log("Object: " + spawnedObjects[i].name + " from room " + i);
            }

            currentRoom++;
            if (currentRoom == nRooms){currentRoom = 0;}
        }
        
    }
}
