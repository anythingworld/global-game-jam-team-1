﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{
    private bool objectClicked = false;
    private MemoryBox memoryBoxScript;
    
    void Start()
    {
        memoryBoxScript = GameObject.Find("MemoryBox").GetComponent<MemoryBox>();
    }

    private void OnMouseDown() {
        memoryBoxScript.boxOpen = false;
        memoryBoxScript.objectClicked = true;
        memoryBoxScript.clickedObjectRoom = gameObject.tag;
        //Destroy(gameObject);
        Debug.Log("Object clicked");
    }

}
