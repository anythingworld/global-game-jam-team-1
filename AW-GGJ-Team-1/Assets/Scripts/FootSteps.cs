﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootSteps : MonoBehaviour
{
    public AudioSource footSource;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Vertical"))
        {
            if(!footSource.isPlaying)
            {
                footSource.Play();
            }
        }
        else
        {
            if(footSource.isPlaying)
            {
                footSource.Stop();
            }
        }
    }
}
