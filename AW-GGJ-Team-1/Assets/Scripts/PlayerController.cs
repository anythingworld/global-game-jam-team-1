﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float speed = 5.0f;
    private float frontBound = 4.0f;
    //private float backBound = 8.0f;
    private GameObject camPivotPoint;
    private Vector3 prevPlayerPos;
    // Start is called before the first frame update
    void Start()
    {
        camPivotPoint = GameObject.Find("PivotPoint");
        prevPlayerPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pivotPoint = camPivotPoint.transform.position;
        float x = transform.position.x;
        float z = transform.position.z;

        float radialTranslate = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        Vector3 playerRadialVect = new Vector3(x - pivotPoint.x, 0, z - pivotPoint.z);
        if (playerRadialVect.magnitude <= frontBound) // || playerRadialVect.magnitude >= backBound)
        {
            transform.position = prevPlayerPos;
            Debug.Log("Passed limits");
        }
        else {
            prevPlayerPos = transform.position;
        }
        transform.Translate(playerRadialVect.normalized * radialTranslate, Space.World);
        
        float horizTranslate = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        Vector3 playerAngularVect = Quaternion.AngleAxis(90, Vector3.up) * playerRadialVect;
        camPivotPoint.transform.Rotate(Vector3.up, playerAngularVect.magnitude * horizTranslate);
        
    }
}
