﻿using UnityEngine;

public class CarryTarget : MonoBehaviour
{
    #region Fields
    public float ThrowSpeed = 20f;
    public float TurnSpeed = 20f;
    public float TargetRadius = 1f;


    public PathConfig config;

    public AWObj targetController = null;
    public Transform TargetTransform = null;
    private Vector3 targetPos;
    private Vector3 startPos;

    private Transform AWThingTransform;
    private AWObj carriedAWObj;

    private Transform headTransform;
    private Transform carriedTransform;

    #endregion

    #region Unity Callbacks

    void Start()
    {
        startPos = transform.position;
        carriedTransform = transform;
    }

    void Update()
    {
        if (TargetTransform == null)
        {
            if (targetController != null)
            {
                if (targetController.AWObjMade == true && targetController.awThing.transform != null) TargetTransform = targetController.awThing.transform;
                carriedAWObj = gameObject.GetComponentInChildren<AWObj>();
                carriedAWObj.ActivateRBs(false);

                Collider[] carriedColliders = gameObject.GetComponentsInChildren<Collider>();
                foreach (Collider carriedCollider in carriedColliders)
                {
                    carriedCollider.enabled = false;
                }

                transform.Rotate(30, 0, 90);

                Transform[] targetTransforms = targetController.GetComponentsInChildren<Transform>();
                foreach (Transform t in targetTransforms)
                {
                    if (t.name == "head")
                    {
                        Renderer[] objRenderers = t.GetComponentsInChildren<Renderer>();
                        Bounds bounds = new Bounds(t.position, Vector3.zero);
                        foreach (Renderer objRenderer in objRenderers)
                        {
                            bounds.Encapsulate(objRenderer.bounds);
                            GameObject headCube = new GameObject();
                            headCube.transform.position = bounds.center + bounds.extents;
                            headCube.transform.parent = t;
                            TargetTransform = headCube.transform;
                        }
                    }
                }

                Transform[] thisTransforms = transform.GetComponentsInChildren<Transform>();
                foreach (Transform t in thisTransforms)
                {
                    if (t.tag == "AWThing")
                    {
                        carriedTransform = t;
                        startPos = carriedTransform.position;
                    }
                }
            }

        }
        else
        {
            SolveMovement();
        }
    }
    #endregion

    #region Private Methods
    private void SolveMovement()
    {
        targetPos = TargetTransform.position;

        // Update position to target
        Vector3 m_DirToTarget = targetPos - carriedTransform.position;

        DrawArrow.ForDebug(carriedTransform.position, m_DirToTarget, Color.red, 2f);

        Vector3 currentPos = Vector3.Lerp(carriedTransform.position, targetPos, 0.9f);

        carriedTransform.position = targetPos;
        carriedTransform.eulerAngles = TargetTransform.eulerAngles - new Vector3(0, 90, -90);
    }
    #endregion

    #region Public Methods
    #endregion
}