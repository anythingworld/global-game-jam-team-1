﻿using System.Collections.Generic;
using UnityEngine;

public class MultilegCrawlerWalkAnimation : WalkAnimation
{

    // Behaviour settings
    public float rightFrontLegMovementSpeed = 4;
    public float leftFrontLegMovementSpeed = 4;
    public float rightHindLegMovementSpeed = 4;
    public float leftHindLegMovementSpeed = 4;
    public float leftMiddleLegMovementSpeed = 4;
    public float rightMiddleLegMovementSpeed = 4;
    public float rightFrontLegMovementRadius = 0.6f;
    public float leftFrontLegMovementRadius = 0.6f;
    public float rightHindLegMovementRadius = 0.6f;
    public float leftHindLegMovementRadius = 0.6f;
    public float leftMiddleLegMovementRadius = 0.6f;
    public float rightMiddleLegMovementRadius = 0.6f;
    public float rightFrontLegStartDegree = 0;
    public float leftFrontLegStartDegree = 180;
    public float rightHindLegStartDegree = 0;
    public float leftHindLegStartDegree = 180;
    public float leftMiddleLegStartDegree = 0;
    public float rightMiddleLegStartDegree = 180;


    // ParameterController object used for monitoring parameters' changes
    ParameterController paramControl;


    // Start is called before the first frame update
    void Start()
    {

        // Obtain settings for this particular animation
        Initialization("multileg_crawler", "walk");

        // Initialize list of parameters used in this behaviour script
        parametersList = new List<Parameter>()
        {
            new Parameter("feet_front_right", "rightFrontLegMovementSpeed","AWFeetMovement", "StepSpeed",rightFrontLegMovementSpeed),
            new Parameter("feet_front_right", "rightFrontLegStartDegree","AWFeetMovement","StepDegree",rightFrontLegStartDegree),
            new Parameter("feet_front_right", "rightFrontLegMovementRadius","AWFeetMovement","StepRadius",rightFrontLegMovementRadius),
            new Parameter("feet_front_left", "leftFrontLegMovementSpeed","AWFeetMovement","StepSpeed",leftFrontLegMovementSpeed),
            new Parameter("feet_front_left", "leftFrontLegStartDegree","AWFeetMovement","StepDegree",leftFrontLegStartDegree),
            new Parameter("feet_front_left", "leftFrontLegMovementRadius","AWFeetMovement","StepRadius",leftFrontLegMovementRadius),
            new Parameter("feet_hind_right", "rightHindLegMovementSpeed","AWFeetMovement","StepSpeed",rightHindLegMovementSpeed),
            new Parameter("feet_hind_right", "rightHindLegStartDegree","AWFeetMovement","StepDegree",rightHindLegStartDegree),
            new Parameter("feet_hind_right", "rightHindLegMovementRadius","AWFeetMovement","StepRadius",rightHindLegMovementRadius),
            new Parameter("feet_hind_left", "leftHindLegMovementSpeed","AWFeetMovement","StepSpeed",leftHindLegMovementSpeed),
            new Parameter("feet_hind_left", "leftHindLegStartDegree","AWFeetMovement","StepDegree",leftHindLegStartDegree),
            new Parameter("feet_hind_left", "leftHindLegMovementRadius","AWFeetMovement","StepRadius",leftHindLegMovementRadius),
            new Parameter("feet_middle_left", "leftMiddleLegMovementSpeed","AWFeetMovement","StepSpeed",leftMiddleLegMovementSpeed),
            new Parameter("feet_middle_left", "leftMiddleLegStartDegree","AWFeetMovement","StepDegree",leftMiddleLegStartDegree),
            new Parameter("feet_middle_left", "leftMiddleLegMovementRadius","AWFeetMovement","StepRadius",leftMiddleLegMovementRadius),
            new Parameter("feet_middle_right", "rightMiddleLegMovementSpeed","AWFeetMovement","StepSpeed",rightMiddleLegMovementSpeed),
            new Parameter("feet_middle_right", "rightMiddleLegStartDegree","AWFeetMovement","StepDegree",rightMiddleLegStartDegree),
            new Parameter("feet_middle_right", "rightMiddleLegMovementRadius","AWFeetMovement","StepRadius",rightMiddleLegMovementRadius)
        };


        // Initialize paramControl and _prefabToScript variables
        paramControl = new ParameterController(parametersList);



        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        StartCoroutine(WaitForAWObjCompletion());

    }


    /// <summary>
    /// Method used for updating all parameters for the behaviour
    /// </summary>
    protected override void UpdateParameters()
    {
        // Check which parameters were modified
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float)>() { ("rightFrontLegMovementSpeed",rightFrontLegMovementSpeed),
            ("rightFrontLegStartDegree",rightFrontLegStartDegree),("rightFrontLegMovementRadius",rightFrontLegMovementRadius),
            ("leftFrontLegMovementSpeed",leftFrontLegMovementSpeed),("leftFrontLegStartDegree",leftFrontLegStartDegree),
            ("leftFrontLegMovementRadius",leftFrontLegMovementRadius), ("rightHindLegMovementSpeed",rightHindLegMovementSpeed),
            ("rightHindLegStartDegree",rightHindLegStartDegree),("rightHindLegMovementRadius",rightHindLegMovementRadius),
            ("leftHindLegMovementSpeed",leftHindLegMovementSpeed), ("leftHindLegStartDegree",leftHindLegStartDegree),
            ("leftHindLegMovementRadius",leftHindLegMovementRadius), ("leftMiddleLegMovementSpeed",leftMiddleLegMovementSpeed), ("leftMiddleLegStartDegree",leftMiddleLegStartDegree),
            ("leftMiddleLegMovementRadius",leftMiddleLegMovementRadius), ("rightMiddleLegMovementSpeed",rightMiddleLegMovementSpeed), ("rightMiddleLegStartDegree",rightMiddleLegStartDegree),
            ("rightMiddleLegMovementRadius",rightMiddleLegMovementRadius)});

        // Update parmeters value in the proper script
        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (scriptsSet)
            UpdateParameters();
    }


    //------------------------------------------------------------------------Behaviour specific methods---------------------------------------------------------------

    /// <summary>
    /// Method used for updating the speed of feet movement in the animation
    /// </summary>
    /// <param name="speed"></param>
    public override void UpdateMovementSpeed(float speed)
    {
        rightFrontLegMovementSpeed = speed;
        leftFrontLegMovementSpeed = speed;
        rightHindLegMovementSpeed = speed;
        leftHindLegMovementSpeed = speed;
        rightMiddleLegMovementSpeed = speed;
        leftMiddleLegMovementSpeed = speed;
    }

    /// <summary>
    /// Method used for updating the size of feet movement in the animation
    /// </summary>
    /// <param name="scale"></param>
    public override void UpdateMovementSizeScale(float scale)
    {
        rightFrontLegMovementRadius *= scale;
        leftFrontLegMovementRadius *= scale;
        rightHindLegMovementRadius *= scale;
        leftHindLegMovementRadius *= scale;
        rightMiddleLegMovementRadius *= scale;
        leftMiddleLegMovementRadius *= scale;
    }

    public override void ActivateShader()
    {
        // throw new System.NotImplementedException();
    }


    public override void DeactivateShader()
    {
        // throw new System.NotImplementedException();
    }
}
