﻿using System.Collections.Generic;
using UnityEngine;

public class VehicleBobAnimation : AWAnimationController
{
    public float bodyShakeMovementSpeed = 0.3f;
    public float bodyShakeMovementFrequency = 0.1f;
    ParameterController paramControl;

    public void Start()
    {
        //TODO: can we make this dynamic?
        Initialization("vehicle_uniform__drive", "drive");

        parametersList = new List<Parameter>()
        {
            new Parameter("body", "bodyShakeMovementSpeed","AWBodyShakeMovement", "MoveSpeed",bodyShakeMovementSpeed),
            new Parameter("body", "bodyShakeMovementFrequency","AWBodyShakeMovement","Frequency",bodyShakeMovementFrequency),
        };
        // Initialize paramControl and _prefabToScript variables
        paramControl = new ParameterController(parametersList);

        // TODO: careful! reliant on parent object script
        controllingAWObj = transform.parent.GetComponent<AWObj>();
        if (controllingAWObj == null)
        {
            controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
            if (controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }
        StartCoroutine(WaitForAWObjCompletion());

    }
    public override void ActivateShader()
    {
        //throw new System.NotImplementedException();
    }

    public override void DeactivateShader()
    {
        //throw new System.NotImplementedException();
    }

    public override void UpdateMovementSpeed(float speed)
    {
        throw new System.NotImplementedException();
    }


    /// <summary>
    /// Method used for updating the size of feet movement in the animation
    /// </summary>
    /// <param name="scale"></param>
    public override void UpdateMovementSizeScale(float scale)
    {
        throw new System.NotImplementedException();
    }

    protected override void UpdateParameters()
    {
        List<Parameter> modifiedParameters = paramControl.CheckParameters(new List<(string, float)>()
        {
            ("bodyShakeMovementSpeed",bodyShakeMovementSpeed),
            ("bodyShakeMovementFrequency",bodyShakeMovementFrequency),
        });

        foreach (var param in modifiedParameters)
        {
            prefabToScript[(param.PrefabPart, param.ScriptName)].ModifyParameter(param);
        }
    }

    private void Update()
    {
        if (scriptsSet)
        {
            UpdateParameters();
        }
    }


}
