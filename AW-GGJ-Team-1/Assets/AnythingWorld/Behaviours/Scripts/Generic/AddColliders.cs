﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class AddColliders : MonoBehaviour
{
    public float scaleParameter = 0.7f;

    private AWObj _controllingAWObj;

    // Dictionary of the colliders created for the model
    private Dictionary<string, Collider> _modelColliders = new Dictionary<string, Collider>();

    // Field used for synchronisation with the feet movement script
    public bool feetSetupComplete { get; set; } = false;

    private Dictionary<string, string> renaming = new Dictionary<string, string>() { { "head", "head_holder" }, { "tail", "tail_holder" }, };

    [SerializeField]
    private bool collidersSet = false;

    private void Start()
    {
        // TODO: careful! reliant on parent object script
        if (transform.parent != null)
        {
            _controllingAWObj = transform.parent.GetComponent<AWObj>();
            if (_controllingAWObj == null)
            {
                Debug.LogError($"No AWObj found for {gameObject.name}");
                return;
            }
        }

        if (!collidersSet)
        {
            StartCoroutine(WaitForAWObjCompletion());
            collidersSet = true;
        }
        else
        {
            feetSetupComplete = true;
        }
    }

    private IEnumerator WaitForAWObjCompletion()
    {
        if (_controllingAWObj != null)
        {
            while (!_controllingAWObj.AWObjMade)
                yield return new WaitForEndOfFrame();
        }
        else
        {
            yield return new WaitForEndOfFrame();
        }

        // Create colliders for the model
        CreateColliders();

        // Setup colliders for model's feet and hands
        FootSetup();
        //HandsSetup();
        feetSetupComplete = true;

        // Set up model's joints
        SetupJoints();

    }

    private void CreateColliders()
    {
        // Collect bounds of all the meshes that construct the object
        var childrenMeshes = transform.GetComponentsInChildren<MeshFilter>();

        for (int i = 0; i < childrenMeshes.Length; i++)
        {
            // Find the parent of the mesh to obtain the scale
            Transform parent = ((MeshFilter)childrenMeshes.GetValue(i)).GetComponentInParent<Transform>().parent;
            Transform prefabParent = parent.parent; // Find the parent that is the prefab element to which the collider is to be added

            // Do not create colliders for wheels
            if (prefabParent.name.IndexOf("wheel") != -1)
                continue;

            // Calculate scaled mesh size and center
            Vector3 scaledSize = Vector3.Scale(new Vector3(scaleParameter, scaleParameter, scaleParameter), Vector3.Scale(((MeshFilter)childrenMeshes.GetValue(i)).sharedMesh.bounds.size, parent.localScale)); //colliders slighthly scaled down so that they do not push each other away
            Vector3 scaledCenter = Vector3.Scale(((MeshFilter)childrenMeshes.GetValue(i)).sharedMesh.bounds.center, parent.localScale);

            // Create collider for the transform 
            if (renaming.ContainsKey(prefabParent.name) && transform.Find(renaming[prefabParent.name]) != null)
                prefabParent = transform.Find(renaming[prefabParent.name]);

            BoxCollider modelCollider = prefabParent.gameObject.AddComponent<BoxCollider>();
            modelCollider.size = scaledSize;
            modelCollider.center = scaledCenter + parent.localPosition;

            _modelColliders.Add(prefabParent.name, modelCollider);
        }
    }

    private void SetupJoints()
    {
        // Collect joints of all the parts that construct the object
        var childrenJoints = transform.GetComponentsInChildren<Joint>();

        for (int i = 0; i < childrenJoints.Length; i++)
        {
            // Get joints anchor and modify its location
            Joint joint = (Joint)childrenJoints.GetValue(i);
            if (_modelColliders.ContainsKey(joint.transform.name))
            {
                float inverseScaleParameter = 1 / scaleParameter;
                joint.anchor = ((BoxCollider)_modelColliders[joint.transform.name]).center + Vector3.Scale(Vector3.Scale(new Vector3(inverseScaleParameter, inverseScaleParameter, inverseScaleParameter), ((BoxCollider)_modelColliders[joint.transform.name]).size), joint.anchor);
            }
            else
            {
                if (joint.transform.childCount == 0)
                    Destroy(joint);
            }


        }
    }

    private void FootSetup()
    {
        // Check if feet colliders have been already created
        var feetcolliders = _modelColliders.Where(kvp => kvp.Key.IndexOf("foot") != -1 || kvp.Key.IndexOf("feet") != -1);
        if (feetcolliders.ToList().Count != 0)
            return;

        // Create feet colliders in case they where not created earlier
        for (int i = 0; i < transform.childCount; i++)
        {
            // Find feet transform
            if (transform.GetChild(i).name.IndexOf("foot") != -1 || transform.GetChild(i).name.IndexOf("feet") != -1)
            {
                // Find which feet it is
                string feetSide = transform.GetChild(i).name.Substring(5);

                // Get bottom part of the leg 
                var legColliders = _modelColliders.Where(kvp => kvp.Key.IndexOf(feetSide) != -1 && kvp.Key.IndexOf("bot") != -1 && kvp.Key.IndexOf("leg") != -1);
                if (legColliders.ToList().Count == 0)
                    legColliders = _modelColliders.Where(kvp => kvp.Key.IndexOf(feetSide) != -1 && kvp.Key.IndexOf("leg") != -1);

                var legCollider = legColliders.First();


                // Specify the position of the feet
                Vector3 position = ((BoxCollider)legCollider.Value).center;
                position.y -= (1 / scaleParameter) * ((BoxCollider)legCollider.Value).size.y;

                // Create collider for the transform 
                SphereCollider modelCollider = transform.GetChild(i).gameObject.AddComponent<SphereCollider>();
                modelCollider.radius = 0.15f;

                _modelColliders.Add(transform.GetChild(i).name, modelCollider);

                //Change position of the feet transform
                transform.GetChild(i).localPosition = position;
            }
        }
    }

    private void HandsSetup()
    {
        // Check if hand colliders have been already created
        var handcolliders = _modelColliders.Where(kvp => kvp.Key.IndexOf("hand") != -1);
        if (handcolliders.ToList().Count != 0)
            return;

        // Create hand colliders in case they where not created earlier
        for (int i = 0; i < transform.childCount; i++)
        {
            // Find hand transform
            if (transform.GetChild(i).name.IndexOf("hand") != -1)
            {
                // Find which hand it is
                string handSide = transform.GetChild(i).name.Substring(5);

                // Get bottom part of the arm 
                var armColliders = _modelColliders.Where(kvp => kvp.Key.IndexOf(handSide) != -1 && kvp.Key.IndexOf("bot") != -1 && kvp.Key.IndexOf("arm") != -1);
                if (armColliders.ToList().Count == 0)
                    armColliders = _modelColliders.Where(kvp => kvp.Key.IndexOf(handSide) != -1 && kvp.Key.IndexOf("arm") != -1);

                var armCollider = armColliders.First();


                // Specify the position of the hand
                Vector3 position = ((BoxCollider)armCollider.Value).center;
                position.y -= (1 / scaleParameter) * ((BoxCollider)armCollider.Value).size.y;

                // Create collider for the transform 
                SphereCollider modelCollider = transform.GetChild(i).gameObject.AddComponent<SphereCollider>();
                modelCollider.radius = 0.15f;

                _modelColliders.Add(transform.GetChild(i).name, modelCollider);

                //Change position of the hand transform
                transform.GetChild(i).localPosition = position;
            }
        }
    }

    public void RemoveColliders()
    {
        foreach (var collider in _modelColliders.Keys)
        {
            AWSafeDestroy.SafeDestroy(_modelColliders[collider]);
        }

        _modelColliders.Clear();
    }

    private void OnDestroy()
    {
        RemoveColliders();
    }
}
