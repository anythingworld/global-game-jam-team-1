﻿using UnityEngine;

public class ObjectMoveTrans : MonoBehaviour
{

    public Vector3 MoveAmount;

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position -= MoveAmount;
    }
}
