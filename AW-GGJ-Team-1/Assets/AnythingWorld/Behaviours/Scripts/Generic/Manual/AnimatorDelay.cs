﻿using UnityEngine;

public class AnimatorDelay : MonoBehaviour
{

    public float aniStart = 0f;
    public bool randomStart = true;

    // Start is called before the first frame update
    void Start()
    {
        if (randomStart)
            aniStart = Random.value;

        GetComponent<Animator>().Play(0, -1, aniStart);
    }
}
