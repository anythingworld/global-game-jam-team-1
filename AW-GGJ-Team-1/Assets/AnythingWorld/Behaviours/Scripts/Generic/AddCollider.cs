﻿using System.Collections;
using UnityEngine;

public class AddCollider : MonoBehaviour
{
    private AWObj _controllingAWObj;

    private void Start()
    {
        // TODO: careful! reliant on parent object script
        // TODO: be forever sad that this code exists - sorry! :) - GM
        if (transform.parent != null)
        {
            _controllingAWObj = transform.parent.GetComponent<AWObj>();
            if (_controllingAWObj == null)
            {
                if (transform.parent.parent != null)
                {
                    _controllingAWObj = transform.parent.parent.GetComponent<AWObj>();
                    if (_controllingAWObj == null)
                    {
                        Debug.LogError($"No AWObj found for {gameObject.name}");
                        return;
                    }
                }
            }
        }

        StartCoroutine(WaitForAWObjCompletion());
    }

    private IEnumerator WaitForAWObjCompletion()
    {
        if (_controllingAWObj != null)
        {
            while (!_controllingAWObj.AWObjMade)
                yield return new WaitForEndOfFrame();
        }
        else
        {
            yield return new WaitForEndOfFrame();
        }

        CreateCollider();
    }

    private void CreateCollider()
    {
        // Collect bounds of all the meshes that construct the object
        Bounds meshCollection = new Bounds();
        var childrenMeshes = transform.GetComponentsInChildren<MeshFilter>();

        for (int i = 0; i < childrenMeshes.Length; i++)
        {
            // Find the parent of the mesh to obtain the scale
            Transform parent = ((MeshFilter)childrenMeshes.GetValue(i)).GetComponentInParent<Transform>().parent;

            // Calculate scaled mesh size and center
            Vector3 scaledSize = Vector3.Scale(((MeshFilter)childrenMeshes.GetValue(i)).sharedMesh.bounds.size, parent.localScale);
            Vector3 scaledCenter = Vector3.Scale(((MeshFilter)childrenMeshes.GetValue(i)).sharedMesh.bounds.center, parent.localScale);

            // Get scaled bounds of the mesh
            Bounds scaledBounds = ((MeshFilter)childrenMeshes.GetValue(i)).sharedMesh.bounds;
            scaledBounds.size = scaledSize;
            scaledBounds.center = scaledCenter + parent.localPosition;

            meshCollection.Encapsulate(scaledBounds);
        }

        // Create collider for the transform 
        BoxCollider modelCollider = transform.gameObject.AddComponent<BoxCollider>();
        modelCollider.size = meshCollection.size;
        modelCollider.center = meshCollection.center;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
