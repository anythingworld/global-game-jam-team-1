﻿using UnityEngine;

public class turntail : MonoBehaviour
{
    // The speed of the vehicle movement
    public float moveSpeed = 2f;

    // Maximum frequency of the shakes
    public int N = 100;

    // Direction of the rotation that simulates the shake of the plane
    float shakeDirection = 1.0f;

    // Helper variable used for the shaking movement
    int timeCount;


    // Start is called before the first frame update
    void Start()
    {
        timeCount = N;
    }

    // Update is called once per frame
    void Update()
    {
        // Define the shake rotation of the plane
        timeCount--;

        // Change the direction of the shake
        if (shakeDirection == 1.0f && timeCount == 0)
            shakeDirection = -1.0f;
        else if (shakeDirection == -1.0f && timeCount == 0)
            shakeDirection = 1.0f;

        // Define the new shake frequency
        if (timeCount == 0)
            timeCount = N;


        transform.position = Vector3.Lerp(transform.position, transform.position + shakeDirection * transform.right, moveSpeed * Time.deltaTime);

    }
}
