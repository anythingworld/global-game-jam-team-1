﻿using System.Collections.Generic;

public static class HabitatMap
{
    public static SortedDictionary<string, HabitatDescription> HabitatList;

    public static void SetUpMaps()
    {
        HabitatList = new SortedDictionary<string, HabitatDescription>();
        // TODO: retrieve from API, hardcoded for now
        // TODO: add thumbnails to editor for habitats
        HabitatList.Add("testing", new HabitatDescription("Test Area", "Just an area for testing. No objects!"));
        HabitatList.Add("underwater", new HabitatDescription("Ocean", "Deep in the ocean is a deep ocean"));
        // HabitatList.Add("cave",new HabitatDescription("Cave","A deeply cavernous environment"));
        // HabitatList.Add("forest",new HabitatDescription("Forest","If a tree falls in a forest don't worry there are plenty more"));
        HabitatList.Add("desert", new HabitatDescription("Desert", "Arid barron landscapes for your dry palette"));
        HabitatList.Add("farm", new HabitatDescription("Farm", "Down on the farm are lots of farm animals"));
        // HabitatList.Add("beach",new HabitatDescription("Beach","Where the land meets the sand"));
        HabitatList.Add("city", new HabitatDescription("City", "Bustling urban landscapes for all your urban needs"));
        // HabitatList.Add("pet",new HabitatDescription("Pets At Home","Ever wanted to keep a pet but not pick up it's poop?"));
        HabitatList.Add("icescape", new HabitatDescription("Arctic", "Cold like ice but frozen and nice"));
        HabitatList.Add("jungle", new HabitatDescription("Jungle", "It's a jungle in here"));
        // HabitatList.Add("lake",new HabitatDescription("Lake","Like the sea but inland and with less water"));
        // HabitatList.Add("pond",new HabitatDescription("Pond","Like the lake but with even less water"));
        // HabitatList.Add("river",new HabitatDescription("River","A fast flowing water feature with plenty of creature"));
        // HabitatList.Add("swamp",new HabitatDescription("Swamp","Soggy ground but creatures abound"));
        // HabitatList.Add("grass",new HabitatDescription("Grassland","On the grass land you will find life to hand"));
        // HabitatList.Add("rural",new HabitatDescription("Countryside","There are actually four sides to the countryside"));
        // HabitatList.Add("urban",new HabitatDescription("Suburbia","Homes in the city"));
        // HabitatList.Add("garden",new HabitatDescription("Garden","Be on your gaurd in your own backyard"));
        HabitatList.Add("space", new HabitatDescription("Outer Space", "Deep in the universe's face hides the human race"));
        // HabitatList.Add("savannah",new HabitatDescription("Savannah","It's not plain on the plane"));
        HabitatList.Add("mountain", new HabitatDescription("Mountain", "The peaks of your achievement"));
        // HabitatList.Add("magical",new HabitatDescription("Magical","It should be fairy obvious what this is"));
        // HabitatList.Add("grassland",new HabitatDescription("Grassland","Lands of grass descend into farce"));
        // HabitatList.Add("sea",new HabitatDescription("Ocean","Deep in the ocean is a deep ocean")); // TODO: add back in with surface
    }

    public static SortedDictionary<string, HabitatDescription> GetHabitats()
    {
        if (HabitatList == null)
            SetUpMaps();

        return HabitatList;
    }
}

public struct HabitatDescription
{
    public string Name;
    public string Description;

    public HabitatDescription(string name, string description)
    {
        Name = name;
        Description = description;
    }
}