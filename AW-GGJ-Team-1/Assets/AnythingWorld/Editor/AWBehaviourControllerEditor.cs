﻿using UnityEditor;
using UnityEngine;
/// <summary>
/// Custom inspector override for the AWBehaviourController script.
/// </summary>
[CustomEditor(typeof(AWBehaviourController))]
public class AWBehaviourControllerEditor : Editor
{
    #region Fields
    private string[] activeScripts = { "" };
    private string[] availableScripts = { "" };
    private int activeIndex = 0;
    private int availableIndex = 0;
    private bool gravityOn;
    private AWBehaviourController controller;
    #endregion

    #region Unity Callbacks
    public void OnEnable()
    {
        //Set controller to be current AWBehaviourController inspector instance.
        controller = (AWBehaviourController)target;
        //Inital refresh of behaviours
        controller.RefreshBehaviours();
        //Initialize active and available behaviour string arrays from controller
        activeScripts = controller.GetActiveBehaviourStrings();
        availableScripts = controller.GetAvailableBehaviourStrings();
        if (controller.transform.childCount > 0)
        {
            if (controller.awThing == null)
            {
                Debug.LogWarning("No AWThing found");
            }
            else if (controller.awThing.TryGetComponent<Rigidbody>(out Rigidbody r))
            {
                gravityOn = r.useGravity;
            }
            else
            {
                Debug.LogWarning("No body found?");
            }
        }
    }
    public override void OnInspectorGUI()
    {
        controller.RefreshBehaviours();
        activeScripts = controller.GetActiveBehaviourStrings();
        availableScripts = controller.GetAvailableBehaviourStrings();

        ///Enable/disable gravity
        gravityOn = EditorGUILayout.Toggle("Use gravity", gravityOn);
        if (gravityOn)
        {
            controller.EnableGravity();
        }
        else
        {
            controller.DisableGravity();
        }

        ///Available behaviour line
        GUILayout.Label("Available Behaviour Scripts");
        GUILayout.BeginHorizontal();
        availableIndex = EditorGUILayout.Popup(availableIndex, availableScripts);
        if (GUILayout.Button("Add Behaviour"))
        {
            controller.AddAWBehaviour(availableScripts[availableIndex]);
            RefreshScripts();
            //Debug.Log("Behaviour Added");
        }
        GUILayout.EndHorizontal();

        ///Currently Active Behaviour Scripts
        GUILayout.Label("Active Behaviour Scripts");
        GUILayout.BeginHorizontal();
        activeIndex = EditorGUILayout.Popup(activeIndex, activeScripts);

        if (GUILayout.Button("Remove Behaviour"))
        {
            controller.RemoveBehaviour(activeScripts[activeIndex]);
            RefreshScripts();
        }
        GUILayout.EndHorizontal();

        //Refresh Behaviour Button
        if (GUILayout.Button("Refresh Behaviours"))
        {
            RefreshScripts();
            Debug.Log("Refreshed behaviours");
        }

        EditorUtility.SetDirty(target);
    }
    #endregion

    #region Public Methods
    public void RefreshScripts()
    {
        controller.RefreshBehaviours();
        activeScripts = controller.GetActiveBehaviourStrings();
        availableScripts = controller.GetAvailableBehaviourStrings();
    }
    #endregion

}