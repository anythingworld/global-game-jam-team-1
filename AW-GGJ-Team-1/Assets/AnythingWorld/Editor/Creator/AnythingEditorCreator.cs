﻿//using Boo.Lang;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
#endif
#if UNITY_EDITOR_WIN
#elif UNITY_EDITOR_OSX
using System.Runtime.InteropServices;
#endif

#if UNITY_EDITOR
#endif
[ExecuteAlways]
/// <summary>
/// Custom editor for the AnythingCreator.
/// </summary>
public class AnythingEditorCreator : AnythingEditor
{
    #region Fields
    private Vector2 scrollPos;
    private GameObject awSetupInstance;
    private string searchTerm;
    //private float results = 0;
    private int displayCount = 0;
    private int searchResultsCount = 0;
    private int navMenuSelected = 0;
    private float panelWidth;
    private float panelHeight;
    private int selectedGridResult = 0;
    List<SearchResult> searchResults = null;
    private int habitatIndex;
    private string voiceInputString = "Example input sentence.";
    [SerializeField]
    private bool playModeEntered = false;
    [NonSerialized]
    bool pMReset = false;
    public bool autoGenerateFromVoice = true;
    private bool zoomedPanelActive = false;
    private bool populateHabitat = true;
    private string tempTextField = "";
    private bool searchReturned = false;
    private bool searchStarted = false;
    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        if (searchResults == null) searchResults = new List<SearchResult>();


        //ShowAPIWindow();

        if (Creator.AWSettings.APIKey == "")
        {
            ShowAPIWindow();
        }
        Setup();
    }

    public static void ShowAPIWindow()
    {
        AnythingAPIGen.OpenFromScript();
    }
    private bool CheckAPI()
    {
        if (AnythingSettings.Instance.APIKey == "")
        {
            if (AnythingSettings.Instance == null)
            {
                AnythingSettings.CreateInstance<AnythingSettings>();
            }

            return false;
        }
        else
        {
            return true;
        }
    }
    public void OnInspectorUpdate()
    {
        Repaint();
    }
    public void OnGUI()
    {

        Voice.SubscribeOutputText(UpdateRecognisedString);
        Voice.SubscribeToListeningStatus(ToggleMicTex);
        Voice.SubscribeToListeningEnded(AutoCreateOnFinish);
        GetWindowSize();
        InitializeResources();
        #region Top Bar
        DrawWindowBanner(AWBanner);
        DrawNavToolbar();

        #endregion

        #region Window Content

        switch (navMenuSelected)
        {
            case 0:
                CreatorPanel();
                break;
            case 1:
                VoicePanel();
                break;
            case 2:
                HabitatPanel();
                break;
            default:
                CreatorPanel();
                break;
        }
        #endregion

        Repaint();
        UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
        UnityEditor.SceneView.RepaintAll();
    }
    #endregion

    #region Private Methods

    private void CreatorPanel()
    {
        if (zoomedPanelActive) GUI.enabled = false;
        EditorGUILayout.BeginVertical();
        //-------------------------------------//
        DrawBoldTextHeader("Create 3D Models");
        //-------------------------------------//
        EditorGUILayout.BeginHorizontal();
        DrawSearchBar(!zoomedPanelActive);
        EditorGUILayout.EndHorizontal();
        //-------------------------------------//
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUIStyle.none, GUI.skin.verticalScrollbar);
        DrawResultsGrid();
        EditorGUILayout.EndScrollView();
        //-------------------------------------//
        EditorGUILayout.EndVertical();

        if (zoomedPanelActive) GUI.enabled = true;
        DrawResultZoomed();
    }
    private void VoicePanel()
    {
        EditorGUILayout.BeginVertical();
        //-------------------------------------//
        //DrawWindowBanner(AWBanner);
        DrawBoldTextHeader("Create Models With Voice");
        //-------------------------------------//
        EditorGUILayout.BeginHorizontal();
        bool micActive = DrawMicButton();
        EditorGUILayout.EndHorizontal();
        //-------------------------------------//
        EditorGUILayout.BeginHorizontal();
        DrawSoundWave(micActive);
        EditorGUILayout.EndHorizontal();
        //-------------------------------------//
        GUILayout.FlexibleSpace();
        DrawSpeechPreview(voiceInputString);
        GUILayout.FlexibleSpace();
        GUILayout.FlexibleSpace();
        //-------------------------------------//
        EditorGUILayout.EndVertical();
    }
    private void HabitatPanel()
    {
        EditorGUILayout.BeginVertical();
        DrawBoldTextHeader("Create Habitat");
        HabitatDropDownMenu();
        //HabitatPreview();
        EditorGUILayout.EndVertical();
    }

    #region Initialization
    /// <summary>
    /// Called when user opens up window from the menu bar.
    /// </summary>
    [MenuItem("Anything World/Anything Creator",false,1)]
    private static void Init()
    {
        UnityEditor.EditorWindow window = GetWindow(typeof(AnythingEditorCreator), false, "Anything Creator");
        window.position = new Rect(100, 100, 250, 100);
        window.Show();
    }

    /// <summary>
    /// Checks if instance of AWSetup exists in scene, creates if not found. Assigns to variable if found.
    /// </summary>
    private void CheckAWSetup()
    {
        if (!GameObject.FindGameObjectWithTag("AWSetup"))
        {
            Instantiate(AnythingSettings.Instance.anythingSetup);
        }
        awSetupInstance = GameObject.FindGameObjectWithTag("AWSetup");
    }

    private void CheckForPlayChange()
    {
        if (searchResults == null) searchResults = new List<SearchResult>();
        if (Application.isPlaying && pMReset == false)
        {
            zoomedPanelActive = false;
            searchTerm = "";
            searchResults.Clear();
            pMReset = true;
            playModeEntered = true;

        }
        if (!Application.isPlaying && playModeEntered == true)
        {
            /*
            if (searchTerm != null)
            {
                AnythingCreator.Instance.RequestSearchResults(searchTerm, UpdateSearchResults);
            }
            */
            playModeEntered = false;
            zoomedPanelActive = false;
            searchTerm = "";
            searchResults.Clear();
        }
    }

    public void GetWindowSize()
    {
        GetWindow<AnythingEditorCreator>("position", false);
        Rect r = position;
        panelWidth = position.width;
        panelHeight = position.height;
    }

    public void UpdateSearchResults(SearchResult[] results)
    {
        searchStarted = true;
        searchResults = new List<SearchResult>();
        if (results.Length > 0)
        {
            searchReturned = true;
            searchResults = results.ToList<SearchResult>();
        }
        else
        {
            searchReturned = false;
        }

    }
    public void UpdateRecognisedString(string updateText)
    {
        voiceInputString = updateText;
    }
    #endregion

    #region Draw Elements

    /// <summary>
    /// Draws Anything world logo and Reset All button for the Anything Creator window.
    /// </summary>
    /// <param name="bannerIcon"> Anything world banner texture to display in top left.</param>
    /// <param name="horizontalPadding"></param>
    /// <param name="verticalPadding"></param>
    private void DrawWindowBanner(Texture bannerIcon, int horizontalPadding = 10, int verticalPadding = 10)
    {
        SpaceWrapped(verticalPadding);
        Rect r = EditorGUILayout.BeginHorizontal();
        SpaceWrapped(horizontalPadding);
        ToggleGUIBackgroundClear();
        GUILayout.Button(bannerIcon, GUIStyle.none);
        ToggleGUIBackgroundClear();
        //DrawTransparentTex(r.x+15, r.y + 9 - creatorButton.height / 2, bannerIcon, false);
        GUILayout.FlexibleSpace();


        if (GUILayout.Button(clearIcon, resetAllButtonStyle, GUILayout.ExpandWidth(false)))
        {
            if (EditorUtility.DisplayDialog("Reset Creations", "Are you sure you want to reset all creations in the scene?", "Reset", "Cancel"))
            {
                ResetEverything();
            }
        }
        SpaceWrapped(horizontalPadding);
        EditorGUILayout.EndHorizontal();
        SpaceWrapped(verticalPadding);
    }




    /// <summary>
    /// Handles search input from user and calls request to API in anything creator.
    /// </summary>
    /// <param name="active">Is search bar active and can accept input</param>
    /// <returns></returns>
    private string DrawSearchBar(bool active)
    {
        if (active)
        {
            string previousSearch = searchTerm;
            //searchTerm = EditorGUILayout.DelayedTextField(searchTerm, inputFieldStyle, GUILayout.Height(30));

            if (Event.current.Equals(Event.KeyboardEvent("return")))
            {
                if (CheckAPI() == false)
                {
                    ShowAPIWindow();
                    return null;
                }

                searchTerm = tempTextField;
                searchResults = new List<SearchResult>();
                //AnythingCreator.Instance.RequestCategorySearchResults(searchTerm, UpdateSearchResults);
                if (searchTerm == "")
                {
                    searchStarted = false;
                    searchReturned = false;
                }
                else
                {
                    AnythingCreator.Instance.RequestCategorySearchResults(searchTerm, UpdateSearchResults);
                }

            }

            tempTextField = EditorGUILayout.TextField(tempTextField, inputFieldStyle, GUILayout.Height(30));


            if (GUILayout.Button("Search", searchButtonStyle, GUILayout.MaxWidth(60)))
            {
                if (CheckAPI() == false)
                {
                    ShowAPIWindow();
                    return null;
                }

                searchTerm = tempTextField;
                searchResults = new List<SearchResult>();


                if (searchTerm == "")
                {
                    searchStarted = false;
                    searchReturned = false;
                }
                else
                {
                    AnythingCreator.Instance.RequestCategorySearchResults(searchTerm, UpdateSearchResults);
                }
            }
            return searchTerm;
        }
        else
        {
            GUILayout.Label(searchTerm, inputFieldStyle, GUILayout.Height(30));
            return searchTerm;
        }
    }

    private void DrawResultsGrid()
    {
        if (searchStarted == true)
        {
            if (searchReturned == true)
            {
                #region Initialise result properties
                if (searchResults == null) { searchResults = new List<SearchResult>(); }
                float resultsPerLine = Mathf.Floor(panelWidth / resultPicStyle.fixedWidth + resultPicStyle.margin.left * 2);
                float rows = 0;
                searchResultsCount = searchResults.Count;
                if (resultsPerLine != 0)
                {
                    rows = searchResultsCount / resultsPerLine;
                }
                displayCount = 0;
                #endregion

                #region Draw Grid

                for (int y = 0; y < rows; y++)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(inputFieldStyle.margin.left);
                    GUILayout.FlexibleSpace();
                    for (int i = 0; i < resultsPerLine; i++)
                    {
                        EditorGUILayout.BeginVertical();
                        if (displayCount < searchResultsCount)
                        {
                            //If there is a result to display in this slot, display.
                            #region Init label and thumnail
                            string resultString = searchResults[displayCount].DisplayName;
                            Texture2D displayThumbnail = searchResults[displayCount].Thumbnail;
                            if (displayThumbnail != null)
                            {
                                displayThumbnail = searchResults[displayCount].Thumbnail;
                            }
                            else
                            {
                                displayThumbnail = placeholderTexture;
                            }
                            #endregion

                            #region Draw Thumnail
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            if (GUILayout.Button(displayThumbnail, resultPicStyle))
                            {
                                //OBJECT CREATION CALLED HERE
                                selectedGridResult = displayCount;
                                AnythingCreator.Instance.MakeObject(searchResults[selectedGridResult].name);
                                //zoomedPanelActive = !zoomedPanelActive;

                            }
                            GUILayout.FlexibleSpace();
                            EditorGUILayout.EndHorizontal();
                            #endregion

                            #region Draw Label
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            GUILayout.Label(resultString, resultLabelStyle);
                            GUILayout.FlexibleSpace();
                            EditorGUILayout.EndHorizontal();
                            #endregion

                            displayCount++;
                        }
                        else
                        {
                            //Draw empty results to make up space in layout.
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            ToggleGUIClear();
                            GUILayout.Label((Texture)null, resultPicStyle);
                            ToggleGUIClear();
                            GUILayout.FlexibleSpace();
                            EditorGUILayout.EndHorizontal();
                            //-------------------------------------//
                            EditorGUILayout.BeginHorizontal();
                            GUILayout.FlexibleSpace();
                            GUILayout.Label("", resultLabelStyle);
                            GUILayout.FlexibleSpace();
                            EditorGUILayout.EndHorizontal();
                            //-------------------------------------//
                            GUILayout.FlexibleSpace();

                        }
                        EditorGUILayout.EndVertical();
                    }

                    GUILayout.Space(inputFieldStyle.margin.left);
                    EditorGUILayout.EndHorizontal();
                }
                #endregion
            }
            else
            {
                DrawResultsNotFound();
            }
        }


    }

    private void DrawResultsNotFound()
    {
        GUILayout.FlexibleSpace();
        DrawBoldTextHeader("No results found.");
        GUILayout.FlexibleSpace();
    }
    private void DrawNavToolbar()
    {
        GUILayout.BeginHorizontal();
        try
        {
            DrawNavButton(creatorButton, 0, "Object creation window");
            DrawNavButton(voiceButton, 1, "Voice creation window");
            DrawNavButton(habitatButton, 2, "Habitat creation window");
        }
        catch
        {

        }
        GUILayout.EndHorizontal();
    }
    private void DrawNavButton(Texture2D texture, int buttonOption, string tooltip)
    {
        guiColor = GUI.backgroundColor;
        GUIStyle buttonStyle;
        if (navMenuSelected == buttonOption)
        {
            GUI.backgroundColor = Color.grey;
            buttonStyle = activeButtonStyle;
        }
        else
        {
            buttonStyle = defaultButtonStyle;
        }
        if (GUILayout.Button(new GUIContent("", texture, tooltip), buttonStyle, GUILayout.MinWidth(1)))
        {
            navMenuSelected = buttonOption;
        }
        GUI.backgroundColor = guiColor;
    }

    private bool DrawMicButton()
    {
        Texture2D activeMicTex = redMicIcon;
        Texture2D restingMicTex = greyMicIcon;

        bool active = false;
        ToggleGUIBackgroundClear();
        GUILayout.FlexibleSpace();


        if (micButtonTex == null) micButtonTex = restingMicTex;
        if (GUILayout.Button(micButtonTex, micButtonStyle, GUILayout.MaxWidth(micButtonTex.width)))
        {
            if (CheckAPI() == false)
            {
                ShowAPIWindow();
                return false;
            }


            AnythingVoiceCreator.Instance.StartVoiceInput();
            Repaint();

        }
        GUILayout.FlexibleSpace();
        ToggleGUIBackgroundClear();
        if (micButtonTex == activeMicTex) active = true;
        return active;
    }

    public IEnumerator StupidStupidHack()
    {
        double startTime = EditorApplication.timeSinceStartup;
        double manualTimeout = 5;
        AnythingEditorCreator window = (AnythingEditorCreator)GetWindow<AnythingEditor>();
        while (EditorApplication.timeSinceStartup - startTime <= manualTimeout)
        {

        }
        yield return null;
    }
    public static void ToggleMicTex(bool listening)
    {
        if (Voice.ListenForVoice)
        {
            micButtonTex = redMicIcon;
        }
        else
        {
            micButtonTex = greyMicIcon;
        }
    }

    private void DrawSoundWave(bool active)
    {
        GUILayout.FlexibleSpace();
        if (active == true)
        {
            GUILayout.Label(soundWavesGreen, centeredLabelStyle);
        }
        else
        {
            GUILayout.Label(soundWavesGrey, centeredLabelStyle);
        }
        GUILayout.FlexibleSpace();
        Voice.GetMaxLevel();
    }

    private void DrawSpeechPreview(string inputString)
    {
        DrawBoldTextHeader("Output");
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label(speechBubble);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        //-------------------------------------//
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        centeredLabelStyle.font = POPPINS_REGULAR;
        GUILayout.Label(inputString, centeredLabelStyle);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(10);

        #region Create Clear Buttons
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (!autoGenerateFromVoice)
        {
            EditorGUILayout.BeginHorizontal();
            smallButtonStyle.normal.textColor = GREEN_COLOR;
            if (GUILayout.Button("Create", smallButtonStyle))
            {
                Voice.TryCreate();
            }
            smallButtonStyle.normal.textColor = RED_COLOR;
            if (GUILayout.Button("Clear", smallButtonStyle))
            {
                Voice.ClearOutput();
            }
            EditorGUILayout.EndHorizontal();
        }
        else
        {

            EditorGUILayout.BeginHorizontal();
            GUI.enabled = false;
            smallButtonStyle.normal.textColor = Color.grey;
            GUILayout.Button("Create", smallButtonStyleDeactivated);
            smallButtonStyle.normal.textColor = Color.grey;
            GUILayout.Button("Clear", smallButtonStyleDeactivated);
            GUI.enabled = true;
            EditorGUILayout.EndHorizontal();
        }

        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        #endregion

        #region Auto Generate Toggle
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        autoGenerateFromVoice = GUILayout.Toggle(autoGenerateFromVoice, "Auto", toggleStyle);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
        #endregion


    }


    private void DrawResultZoomed()
    {
        if (zoomedPanelActive)
        {
            DrawTex(0, 0, backgroundBlur, true, panelWidth, panelHeight, 0.5f);
            float zoomedPanelWidth = panelWidth * 0.6f;
            float zoomedPanelHeight = panelHeight * 0.6f;
            float panelX = (panelWidth / 2) - (zoomedPanelWidth / 2);
            float panelY = (panelHeight / 2) - (zoomedPanelHeight / 2);

            float arrowPanelWidth = 100;
            Rect leftArrowPanel = new Rect(panelX - arrowPanelWidth, panelY, arrowPanelWidth, zoomedPanelHeight);
            GUILayout.BeginArea(leftArrowPanel);
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            if (selectedGridResult == 0)
            {

            }
            else
            {
                if (GUILayout.Button(leftArrow, zoomBackButton, GUILayout.Height(leftArrowPanel.height)))
                {
                    if (selectedGridResult == 0)
                    {

                    }
                    else
                    {
                        selectedGridResult--;
                    }
                }
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
            GUILayout.EndArea();

            Rect displayPanel = new Rect(panelX, panelY, zoomedPanelWidth, zoomedPanelHeight);
            DrawTex(displayPanel, backgroundBlur, 0.95f);

            GUILayout.BeginArea(displayPanel);
            GUILayout.BeginHorizontal();
            GUILayout.Space(50);
            DrawBoldTextHeader(searchResults[selectedGridResult].DisplayName);
            if (GUILayout.Button(backButton, zoomBackButton, GUILayout.Height(50), GUILayout.Width(50)))
            {
                zoomedPanelActive = false;
            }
            GUILayout.EndHorizontal();

            GUILayout.FlexibleSpace();
            GUILayout.Label(searchResults[selectedGridResult].thumbnail, zoomBackButton, GUILayout.ExpandHeight(true), GUILayout.MinHeight(1));
            GUILayout.FlexibleSpace();
            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            smallButtonStyle.normal.textColor = GREEN_COLOR;

            if (GUILayout.Button("Create", smallButtonStyle))
            {
                AnythingCreator.Instance.MakeObject(searchResults[selectedGridResult].name);

                //newObjectCoroutine = EditorCoroutineUtility.StartCoroutineOwnerless(WaitTilMake(typesShown, Vector3.zero, Quaternion.identity, Vector3.one, true, true));
            }

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(5);
            GUILayout.FlexibleSpace();
            GUILayout.EndArea();

            Rect rightArrowPanel = new Rect(panelX + zoomedPanelWidth, panelY, arrowPanelWidth, zoomedPanelHeight);
            GUILayout.BeginArea(rightArrowPanel);
            GUILayout.BeginVertical();
            if (selectedGridResult == searchResults.Count - 1)
            {

            }
            else
            {
                if (GUILayout.Button(rightArrow, zoomBackButton, GUILayout.Height(rightArrowPanel.height)))
                {
                    if (selectedGridResult < searchResults.Count - 1)
                    {
                        selectedGridResult++;
                    }
                }
            }

            GUILayout.EndVertical();
            GUILayout.EndArea();

        }
    }


    private void HabitatDropDownMenu()
    {

        string[] options = Habitat.GetHabitatWithDefault();

        habitatIndex = EditorGUILayout.Popup(habitatIndex, options, dropDownStyle, GUILayout.Height(50));
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        smallButtonStyle.normal.textColor = GREEN_COLOR;
        if (GUILayout.Button("Create", smallButtonStyle))
        {
            if (habitatIndex != 0)
            {
                Habitat.CreateHabitat(habitatIndex - 1, populateHabitat);
            }
        }
        smallButtonStyle.normal.textColor = RED_COLOR;
        if (GUILayout.Button("Clear", smallButtonStyle))
        {
            habitatIndex = 0;
        }
        populateHabitat = GUILayout.Toggle(populateHabitat, "Populate Habitat", toggleStyle);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
    }

    private void HabitatPreview()
    {
        string[] options = Habitat.GetHabitatOptions();


        if (options == null)
        {
            options = new string[] { "Select Habitat" };
        }


        GUILayout.FlexibleSpace();
        DrawBoldTextHeader("Current Habitat: " + options[habitatIndex]);
        EditorGUILayout.BeginHorizontal();

        GUILayout.Label(globeIcon, zoomBackButton);
        EditorGUILayout.EndHorizontal();
        GUILayout.FlexibleSpace();
    }
    #endregion

    #region Utility Functions
    public void AutoCreateOnFinish()
    {
        if (autoGenerateFromVoice == true)
        {
            Voice.TryCreate();
        }
    }
    #endregion


    #endregion
}