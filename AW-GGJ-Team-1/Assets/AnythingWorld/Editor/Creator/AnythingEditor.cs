﻿using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using System.Collections;
using System.Collections.Generic;
using System;





/// <summary>
/// Custom Editor window for the anything editor.
/// </summary>
[Serializable]
public class AnythingEditor : EditorWindow
{
    #region Fields
    public const int OBJECT_SPACING = 10;
    public static int OBJECT_PADDING = 1;
    public static Color GREEN_COLOR = new Color(0.53f, 1f, 0f);
    public static Color BLUE_COLOR = new Color(0.20000f, 0.68627f, 0.90588f);
    public static Color RED_COLOR = new Color(1.0f, 0.3f, 0.2f);
    public static Color guiColor;


    public enum PoppinsStyle
    {
        Regular,
        Bold,
        Medium
    }

    #region Scene Variables
    [SerializeField]
    public List<int> objClones;
    [SerializeField]
    public int ObjTypes = 0;
    [SerializeField]
    public List<string> objName;
    [SerializeField]
    protected List<int> currClones;
    [SerializeField]
    protected List<string> currName;
    [SerializeField]
    private string objNameLower;
    [SerializeField]
    private AWObj currObject;
    [SerializeField]
    private List<GameObject> currObjectGO;
    [SerializeField]
    private List<AWClones> awClones;
    [SerializeField]
    protected EditorCoroutine newObjectCoroutine;
    [SerializeField]
    protected EditorCoroutine newHabitatCoroutine;
    [SerializeField]
    protected List<bool> readyToClone;
    [SerializeField]
    protected bool setup;
    [SerializeField]
    private Flock groupController;
    [SerializeField]
    private GameObject groupControllerObject;
    [SerializeField]
    private string category;
    [SerializeField]
    private string behaviour;
    [SerializeField]
    private int totalObjects;
    [SerializeField]
    public static AnythingSetup setupInstance;
    #endregion


    [SerializeField]
    public static AnythingVoiceCreator Voice
    {
        get
        {
            return AnythingVoiceCreator.Instance;
        }
    }
    [SerializeField]
    public static AnythingEditorHabitatCreator Habitat
    {
        get
        {
            return AnythingEditorHabitatCreator.Instance;
        }
    }
    [SerializeField]
    public static AnythingCreator Creator
    {
        get
        {
            return AnythingCreator.Instance;
        }
    }
    [SerializeField]
    public static SceneLedger SceneLedger
    {
        get
        {
            return Creator.SceneLedger;
        }
    }
    public static void ForceCloseScenePanel()
    {
        AnythingEditorScenePanel sceneWindow = (AnythingEditorScenePanel)EditorWindow.GetWindowWithRect(typeof(AnythingEditorScenePanel), new Rect(0, 0, 100, 150));
        sceneWindow.Close();
    }
    #endregion

    #region Unity Callbacks
    void OnInspectorUpdate()
    {
        Repaint();
    }

    #endregion

    private void Awake()
    {
        Setup();
    }



    #region Public Methods
    /// <summary>
    /// Initialize anything editor variables and load logo resources.
    /// </summary>
    public void Setup()
    {
        if (!setup)
        {
            objClones = new List<int>();
            currClones = new List<int>();
            objName = new List<string>();
            objName.Add("");
            currName = new List<string>();
            readyToClone = new List<bool>();
            currObjectGO = new List<GameObject>();
            ObjTypes = 1;
            awClones = new List<AWClones>();
            setup = true;
            totalObjects = 0;
            //Debug.Log("Anything Creator Reset");
            Repaint();
        }
        MakeListsReady();
        //InitializeResources();

    }
    /// <summary>
    /// Batch calls the <see cref="InitializeFonts"/> , <see cref="InitializeTextures"/> , and <see cref="InitializeCustomStyles"/> functions.
    /// </summary>
    public void InitializeResources()
    {
        InitializeFonts();
        InitializeTextures();
        InitializeCustomStyles();
    }

    /// <summary>
    /// TODO: What does this do? Can we remove.
    /// </summary>
    public new void Show()
    {
        base.Show();
        SetUpFresh();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Calls setup function that resets variables.
    /// </summary>
    protected void SetUpFresh()
    {
        setup = false;
        Setup();
    }
    /// <summary>
    /// Create group of AW objects, currently either flying/swimming.
    /// </summary>
    /// <param name="typeIndex"></param>
    private void CreateAWGroup(int typeIndex)
    {

        GameObject groupObj = GroupMap.GroupPrefab(behaviour);

        groupControllerObject = Instantiate(groupObj) as GameObject;
        // TODO: properly
        groupController = groupControllerObject.GetComponent<Flock>();

        // TODO: polymorphic, proper
        FlockMember currFlockMember = currObjectGO[typeIndex].GetComponentInChildren<FlockMember>();
        groupController.flockPrefabs = new GameObject[1];
        groupController.flockPrefabs[0] = currFlockMember.gameObject; // TODO: allow for more fish  
        AddCloneToGroup(currFlockMember.gameObject, typeIndex);

        // we have already added our original object to group, so can safely destroy AWObj gameobject now
        AWSafeDestroy.SafeDestroy(currObjectGO[typeIndex]);

        currObjectGO[typeIndex] = currFlockMember.gameObject;
        awClones[typeIndex].ClonedThings[0] = currFlockMember.gameObject;
    }

    /// <summary>
    /// Adds extra object to same group.
    /// </summary>
    /// <param name="groupClone">New clone object.</param>
    /// <param name="typeIndex">Index relating to type of group.</param>
    private void AddCloneToGroup(GameObject groupClone, int typeIndex)
    {
        FlockMember flockChild = groupClone.GetComponentInChildren<FlockMember>();

        // TODO: careful, this will pick up any Flock
        if (groupController == null)
            groupController = GameObject.FindObjectOfType<Flock>();

        groupController.AddMember(flockChild);
    }

    /// <summary>
    /// Remove clones of an object from its group.
    /// </summary>
    /// <param name="removeTotal">Number of objects to remove.</param>
    private void RemoveClonesFromGroup(int removeTotal)
    {
        groupController.numMembers -= removeTotal;
    }

    /// <summary>
    /// Coroutine to create AWObjects.
    /// </summary>
    /// <param name="typeIndex"></param>
    /// <param name="objPosition"></param>
    /// <param name="objRotation"></param>
    /// <param name="objScale"></param>
    /// <param name="hasBehaviour"></param>
    /// <param name="useGrid"></param>
    /// <returns></returns>
    private IEnumerator CreateAWObject(int typeIndex, Vector3 objPosition, Quaternion objRotation, Vector3 objScale, bool hasBehaviour, bool useGrid)
    {
        bool isHabitatCreation = this.GetType().Equals(typeof(AnythingEditorHabitatCreator));
        if (!isHabitatCreation || (isHabitatCreation && !AnythingSetup.Instance.IsShowingLoading))
        {
            AnythingSetup.Instance.ShowLoading(true);
        }

        if (typeIndex >= currObjectGO.Count)
        {
            MakeListsReady();
        }

        currObjectGO[typeIndex] = new GameObject();
        currObjectGO[typeIndex].name = objName[typeIndex];
        currObject = currObjectGO[typeIndex].AddComponent<AWObj>();
        currObject.MakeAWObj(objName[typeIndex], hasBehaviour, true);

        // TODO: work out why we get a null reference for clonedthings here
        if (awClones == null)
        {
            SetUpFresh();
            yield break;
        }

        if (awClones.Count <= typeIndex)
        {
            awClones.Add(new AWClones());
        }
        if (awClones[typeIndex].Key != objNameLower)
        {
            awClones[typeIndex].Key = objNameLower;
            awClones[typeIndex].ClonedThings = new List<GameObject>();
            awClones[typeIndex].UniqueIDs = new List<int>();
        }

        awClones[typeIndex].ClonedThings.Add(currObjectGO[typeIndex]);
        awClones[typeIndex].UniqueIDs.Add(currObjectGO[typeIndex].GetInstanceID());

        bool haveError = false;

        while (!currObject.AWObjMade)
        {
            yield return new EditorWaitForSeconds(0.01f);
            if (currObject.AWKeyInvalid)
            {
                EditorUtility.DisplayDialog("API Key Invalid!", "We couldn't validate the key: " + AnythingSettings.Instance.APIKey + " \nPlease double check the API Key entry in AnythingWorld/Settings/AnythingSettings", "Okidoke!");
                haveError = true;
            }
            else if (currObject.AWAppIdInvalid)
            {
                EditorUtility.DisplayDialog("App Name Is Empty!", "We couldn't see an App Name! \nPlease double check the App Name entry in AnythingWorld/Settings/AnythingSettings", "Okidoke!");
                haveError = true;
            }
            else if (currObject.AWObjNotFound)
            {
                EditorUtility.DisplayDialog("Sorry!", "We couldn't find a " + objName[typeIndex] + "!", "Try Something Else!");
                haveError = true;
            }

            if (haveError)
            {
                AnythingSetup.Instance.ShowLoading(false);
                SetUpFresh();
                yield break;
            }
        }

        readyToClone[typeIndex] = true;
        objClones[typeIndex] = currClones[typeIndex] = 1;

        // TODO: add option for custom editor positioning
        if (useGrid)
        {
            Vector3 creatorPos = GetGridPosition(currObject, currObject.ObjectScale);
            currObject.transform.position = creatorPos;
        }
        else
        {
            currObject.transform.position = objPosition;
        }

        currObject.transform.rotation = objRotation;
        currObject.transform.localScale = objScale;

        // add to ObjTypes
        ObjTypes = typeIndex + 2;

        totalObjects++;


        category = CategoryMap.GetCategory(objNameLower);
        behaviour = BehaviourMap.GetBehaviour(objNameLower);

        if (!isHabitatCreation)
        {
            AnythingSetup.Instance.ShowLoading(false);
            AnythingCreator anythingCreator = AnythingCreator.Instance;
        }
    }

    private void ReconstructAllClones()
    {
        foreach (AWClones awClones in awClones)
        {
            string goName = awClones.Key + " Creator";
            awClones.ClonedThings = new List<GameObject>();
            for (int i = 0; i < awClones.UniqueIDs.Count; i++)
            {
                GameObject cloneGO = EditorUtility.InstanceIDToObject(awClones.UniqueIDs[i]) as GameObject;
                awClones.ClonedThings.Add(cloneGO);
            }

        }
    }

    private void RemoveAWObjects(string objectName, int typeIndex)
    {
        GameObject removalTestObject = awClones[typeIndex].ClonedThings[0];
        if (removalTestObject == null)
        {
            ReconstructAllClones(); // on return from play mode...
        }

        int removeTotal = awClones[typeIndex].ClonedThings.Count - objClones[typeIndex];

        if (GroupMap.ThingHasAGroup(behaviour, category))
        {
            RemoveClonesFromGroup(removeTotal);
        }

        int removed = 0;
        int indexToRemove;

        while (removed < removeTotal)
        {
            indexToRemove = awClones[typeIndex].ClonedThings.Count - 1;
            AWSafeDestroy.SafeDestroy(awClones[typeIndex].ClonedThings[indexToRemove]);
            awClones[typeIndex].ClonedThings.RemoveAt(indexToRemove);
            awClones[typeIndex].UniqueIDs.RemoveAt(indexToRemove);
            removed++;
            totalObjects--;
            Creator.LayoutGrid.RemoveLastInstruction();
        }
    }
    private void RemoveGroupController()
    {
        if (groupControllerObject != null)
        {
            AWSafeDestroy.SafeDestroy(groupControllerObject);
        }
    }
    private void GetTotalObjects()
    {
        totalObjects = 0;
        foreach (AWClones awClones in awClones)
        {
            if (awClones.UniqueIDs != null)
                totalObjects += awClones.UniqueIDs.Count;
        }
    }

    // assuming 10x10 grid
    private Vector3 GetGridPosition(AWObj gridObj, float gridScale)
    {
        float yOffset = gridObj.BoundsYOffset;

        Vector3 gridPos = Creator.LayoutGrid.GetNextAvailablePos(gridScale);

        if (Creator.LayoutGrid.NoSpaceLeft)
        {
            Debug.LogWarning("No space left on the grid, so expanding it!");
            Creator.LayoutGrid.ExpandGrid(10, 0);
            gridPos = Creator.LayoutGrid.GetNextAvailablePos(gridScale);
        }
        gridPos.y = yOffset;

        return gridPos;
    }
    #endregion

    #region Protected Methods
    protected void CloneAWObject(string objectName, int typeIndex, bool setPosRot, Vector3 clonePos, float cloneYRot = 0f)
    {
        Vector3 objPosition = new Vector3(0, 0, 0);

        for (int i = currClones[typeIndex]; i < objClones[typeIndex]; i++)
        {
            GameObject objectToClone = awClones[typeIndex].ClonedThings[0];
            if (objectToClone == null)
            {
                ReconstructAllClones(); // on return from play mode...
                // Debug.LogError($"_awClones[{typeIndex}].ClonedThings[0] null error for {_awClones[typeIndex].Key}");
                objectToClone = awClones[typeIndex].ClonedThings[0];
            }

            objPosition = new Vector3(currClones[typeIndex], 0, 0) * 100;
            GameObject newClone = Instantiate(objectToClone, objPosition, objectToClone.transform.rotation);

            FlockMember currFlockMember = newClone.GetComponentInChildren<FlockMember>();

            if (currFlockMember != null)
            {

                AddCloneToGroup(currFlockMember.gameObject, typeIndex);

                if (awClones[typeIndex].Key != objNameLower)
                {
                    awClones[typeIndex].Key = objNameLower;
                    awClones[typeIndex].ClonedThings = new List<GameObject>();
                    awClones[typeIndex].UniqueIDs = new List<int>();
                }

                awClones[typeIndex].ClonedThings.Add(currFlockMember.gameObject);
                awClones[typeIndex].UniqueIDs.Add(currFlockMember.gameObject.GetInstanceID());

            }
            else
            {
                if (awClones[typeIndex].Key != objNameLower)
                {
                    awClones[typeIndex].Key = objNameLower;
                    awClones[typeIndex].ClonedThings = new List<GameObject>();
                    awClones[typeIndex].UniqueIDs = new List<int>();
                }
            }
            awClones[typeIndex].ClonedThings.Add(newClone);
            awClones[typeIndex].UniqueIDs.Add(newClone.GetInstanceID());

            if (!GroupMap.ThingHasAGroup(behaviour, category))
            {
                int spaceBit = OBJECT_SPACING * i;

                Vector3 cPos = clonePos;
                if (!setPosRot)
                {
                    AWObj cloneAWObj = objectToClone.GetComponent<AWObj>();
                    float cloneScale = cloneAWObj.ObjectScale;
                    cPos = GetGridPosition(cloneAWObj, cloneScale);
                    cPos.y = objectToClone.transform.position.y;
                }
                else
                {
                    Vector3 cloneRot = newClone.transform.eulerAngles;
                    cloneRot.y = cloneYRot;
                    newClone.transform.eulerAngles = cloneRot;
                }

                newClone.transform.position = cPos;

                // TODO: total objects limit!
                // TODO: objects limit for groups / flocks too
                totalObjects++;
            }
        }

    }
    protected void MakeListsReady()
    {
        if (!setup)
            Setup();
        while (objClones.Count < ObjTypes)
        {
            objClones.Add(0);
            currClones.Add(0);
            objName.Add("");
            currName.Add("-");
            readyToClone.Add(false);
            awClones.Add(new AWClones());
            currObjectGO.Add(null);

            // Debug.LogWarning($"MAKEING LISTS LONGER for ObjTypes -> {ObjTypes}");
        }

    }
    protected void DrawUILine(Color color, int thickness = 1, int padding = 20)
    {
        Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
        r.height = thickness;
        r.y += padding / 2;
        r.x -= 2;
        r.width += 6;
        EditorGUI.DrawRect(r, color);
    }

    protected IEnumerator WaitTilMake(int typeIndex, Vector3 objPosition, Quaternion objRotation, Vector3 objScale, bool hasBehaviour, bool useGrid = true)
    {
        objNameLower = objName[typeIndex].ToLower();

        if (objNameLower.Length > 2)
        {
            yield return EditorCoroutineUtility.StartCoroutineOwnerless(CreateAWObject(typeIndex, objPosition, objRotation, objScale, hasBehaviour, useGrid));

            if (GroupMap.ThingHasAGroup(behaviour, category))
            {
                CreateAWGroup(typeIndex);
            }
            // TODO: fix properly by serializing this class properly - GM
            if (typeIndex >= currName.Count)
            {
                // MakeListsReady();
                typeIndex = currName.Count - 1;
            }
            currName[typeIndex] = objName[typeIndex];
        }

        yield break;
    }
    protected void AdjustAWObjects(string objectName, int typeIndex)
    {
        if (AnythingSettings.Instance.showDebugMessages) Debug.Log($"{currClones[typeIndex]} vs {objClones[typeIndex]}");
        if (currClones[typeIndex] < objClones[typeIndex])
        {
            CloneAWObject(objectName, typeIndex, false, Vector3.zero);
        }
        else if (currClones[typeIndex] > objClones[typeIndex])
        {
            RemoveAWObjects(objectName, typeIndex);
        }

        currClones[typeIndex] = objClones[typeIndex];
    }
    protected void ClearAWObjects(string objectName, int typeIndex)
    {
        // if we get an error here, reset whole panel
        if (awClones == null)
            SetUpFresh();

        if (awClones[typeIndex] != null)
        {
            if (awClones[typeIndex].Key == objectName)
            {
                List<GameObject> removals = awClones[typeIndex].ClonedThings;
                foreach (GameObject removalGO in removals)
                {
                    AWSafeDestroy.SafeDestroy(removalGO);
                }
            }
        }

        AWSafeDestroy.SafeDestroy(currObjectGO[typeIndex]);
        objClones[typeIndex] = currClones[typeIndex] = 0;
        currName[typeIndex] = "";
        readyToClone[typeIndex] = false;
        objName[typeIndex] = "";

        // TODO: careful! what if button clicked super quick?
        if (ObjTypes > 1)
        {
            ObjTypes--;
        }

        // back to the start
        if (ObjTypes == 1)
        {
            setup = false;
            Setup();
        }

        RemoveGroupController();
    }
    protected void ResetHabitat()
    {
        AWHabitat[] sceneAWHabitats = FindObjectsOfType<AWHabitat>();
        foreach (AWHabitat awHab in sceneAWHabitats)
        {
            AWSafeDestroy.SafeDestroy(awHab.gameObject);
        }
    }
    protected void ResetAWThings()
    {

    }
    protected void ResetEverything()
    {
        AWObj[] sceneAWObjs = FindObjectsOfType<AWObj>();
        foreach (AWObj awObj in sceneAWObjs)
        {
            AWSafeDestroy.SafeDestroy(awObj.gameObject);
        }
        // reset anything objects
        AnythingObject[] anythingObjects = FindObjectsOfType<AnythingObject>();
        foreach (AnythingObject anythingObject in anythingObjects)
        {
            anythingObject.ObjectCreated = false;
        }
        AWHabitat[] sceneAWHabitats = FindObjectsOfType<AWHabitat>();
        foreach (AWHabitat awHab in sceneAWHabitats)
        {
            AWSafeDestroy.SafeDestroy(awHab.gameObject);
        }
        // remove groups too
        Flock[] sceneFlocks = FindObjectsOfType<Flock>();
        foreach (Flock flockObj in sceneFlocks)
        {
            AWSafeDestroy.SafeDestroy(flockObj.gameObject);
        }
        // anything creators left behind from habitat creation
        AnythingCreator[] anythingCreators = FindObjectsOfType<AnythingCreator>();
        foreach (AnythingCreator anythingCreator in anythingCreators)
        {
            AWSafeDestroy.SafeDestroy(anythingCreator.gameObject);
        }
        Utils.ClearLogConsole();
        AnythingSetup.Instance.ClearAttribution();
        AnythingSetup.Instance.ShowLoading(false);
        SetUpFresh();
    }

    // called from anythingeditor extensions - voice & habitat currently
    protected IEnumerator MakeObject(string objToMake)
    {
        yield return EditorCoroutineUtility.StartCoroutine(MakeObjectProcess(objToMake, Vector3.zero, Quaternion.identity, Vector3.one, true, true), this);
    }

    protected IEnumerator MakeObject(string objToMake, Vector3 objPosition, Quaternion objRotation, Vector3 objScale, bool hasBehaviour)
    {
        yield return EditorCoroutineUtility.StartCoroutine(MakeObjectProcess(objToMake, objPosition, objRotation, objScale, hasBehaviour, false), this);
    }

    protected IEnumerator MakeObjectProcess(string objToMake, Vector3 objPosition, Quaternion objRotation, Vector3 objScale, bool hasBehaviour, bool useGrid)
    {
        if (newObjectCoroutine != null)
            EditorCoroutineUtility.StopCoroutine(newObjectCoroutine);



        // make new
        if (!objName.Contains(objToMake))
        {
            objName[ObjTypes - 1] = objToMake;
            readyToClone.Add(false);
            newObjectCoroutine = EditorCoroutineUtility.StartCoroutine(WaitTilMake(ObjTypes - 1, objPosition, objRotation, objScale, hasBehaviour, useGrid), this);
            yield return newObjectCoroutine;
        }
        else
        {
            int objectIndex = objName.IndexOf(objToMake);
            objClones[objectIndex]++;
            AdjustAWObjects(objToMake, objectIndex);
        }
    }
    protected void AdjustCurrentClones(string objectName, int numClones)
    {
        int objectIndex = objName.IndexOf(objectName);
        objClones[objectIndex] += numClones - 1; // TODO: amend, i think objectIndex always tracks 1 ahead - GM
        AdjustAWObjects(objectName, objectIndex);
    }
    #endregion

    #region Draw Editor Elements

    protected void DrawTransparentTex(float x, float y, Texture icon, bool space = false, float opacity = 0)
    {
        //if (opacity > 1) opacity = 1;
        //if (opacity < 0) opacity = 0;

        int width = icon.width;
        int height = icon.height;
        guiColor = GUI.color;
        GUI.color = Color.clear;

        Color background = GUI.backgroundColor;
        if (space == true) GUILayout.Button("", GUIStyle.none, GUILayout.Width(width), GUILayout.Height(height));

        EditorGUI.DrawTextureTransparent(new Rect(x, y, width, height), icon);
        //GUI.DrawTexture(new Rect(x, y, width, height), icon);
        GUI.color = guiColor;
    }
    protected void DrawTex(float x, float y, Texture icon, float opacity = 1f)
    {
        int width = icon.width;
        int height = icon.height;
        guiColor = GUI.color;
        GUI.color = new Color(1f, 1f, 1f, opacity);
        Color background = GUI.backgroundColor;
        GUI.DrawTexture(new Rect(x, y, width, height), icon, ScaleMode.StretchToFill);
        GUI.color = guiColor;
    }
    protected void DrawTex(float x, float y, Texture icon, bool stretchToPanel, float panelWidth, float panelHeight, float opacity = 1f)
    {
        int width = icon.width;
        int height = icon.height;
        guiColor = GUI.color;
        GUI.color = new Color(1f, 1f, 1f, opacity);
        Color background = GUI.backgroundColor;
        if (stretchToPanel)
        {
            width = (int)panelWidth;
            height = (int)panelHeight;
        }
        if (stretchToPanel) width = (int)panelWidth;
        GUI.DrawTexture(new Rect(x, y, width, height), icon, ScaleMode.StretchToFill);
        GUI.color = guiColor;
    }

    protected void DrawTex(Rect rect, Texture icon, float opacity = 1f)
    {
        guiColor = GUI.color;
        GUI.color = new Color(1f, 1f, 1f, opacity);
        GUI.DrawTexture(rect, icon, ScaleMode.StretchToFill);
        GUI.color = guiColor;
    }

    protected void DrawBoldTextHeader(string text)
    {
        GUILayout.Label(text, titleLabelStyle);
    }


    protected void DrawBoldTextHeader(string text, int fontSize)
    {

        int oldFont = titleLabelStyle.fontSize;
        titleLabelStyle.fontSize = fontSize;
        GUILayout.Label(text, titleLabelStyle);
        titleLabelStyle.fontSize = oldFont;
    }


    protected void DrawBoldTextHeader(string text, int fontSize, TextAnchor anchor)
    {
        int oldFont = titleLabelStyle.fontSize;
        TextAnchor oldAnchor = titleLabelStyle.alignment;

        titleLabelStyle.alignment = anchor;
        titleLabelStyle.fontSize = fontSize;
        GUILayout.Label(text, titleLabelStyle);
        titleLabelStyle.fontSize = oldFont;
        titleLabelStyle.alignment = oldAnchor;
    }
    protected void DrawBoldTextHeader(string text, int fontSize, TextAnchor anchor, bool wrap)
    {
        int oldFont = titleLabelStyle.fontSize;
        TextAnchor oldAnchor = titleLabelStyle.alignment;

        titleLabelStyle.alignment = anchor;
        titleLabelStyle.fontSize = fontSize;
        GUILayout.Label(text, titleLabelStyle);
        titleLabelStyle.fontSize = oldFont;
        titleLabelStyle.alignment = oldAnchor;
        titleLabelStyle.wordWrap = wrap;
    }

    protected void DrawCustomText(string textString, int fontSize, bool wrap, TextAnchor anchor = TextAnchor.MiddleLeft, PoppinsStyle fontStyle = PoppinsStyle.Regular)
    {
        GUIStyle temp = new GUIStyle(EditorStyles.label);
        temp.fontSize = fontSize;
        temp.alignment = anchor;
        temp.font = GetPoppinsFont(fontStyle);
        temp.wordWrap = wrap;
        GUILayout.Label(textString, temp);
    }
    protected void DrawCustomText(string textString, int fontSize, TextAnchor anchor = TextAnchor.MiddleLeft, PoppinsStyle fontStyle = PoppinsStyle.Regular)
    {
        GUIStyle temp = new GUIStyle(EditorStyles.label);
        temp.fontSize = fontSize;
        temp.alignment = anchor;
        temp.font = GetPoppinsFont(fontStyle);

        GUILayout.Label(textString, temp);
    }
    protected void DrawCustomText(string textString, int fontSize, RectOffset margin, TextAnchor anchor = TextAnchor.MiddleLeft, PoppinsStyle fontStyle = PoppinsStyle.Regular)
    {
        GUIStyle temp = new GUIStyle(EditorStyles.label);
        temp.fontSize = fontSize;
        temp.alignment = anchor;
        temp.font = GetPoppinsFont(fontStyle);
        temp.margin = margin;
        GUILayout.Label(textString, temp);
    }
    protected void DrawCustomText(string textString, int fontSize, Color color, TextAnchor anchor = TextAnchor.MiddleLeft, PoppinsStyle fontStyle = PoppinsStyle.Regular)
    {
        GUIStyle temp = new GUIStyle(EditorStyles.label);
        temp.fontSize = fontSize;
        temp.alignment = anchor;
        temp.font = GetPoppinsFont(fontStyle);
        temp.normal.textColor = color;
        GUILayout.Label(textString, temp);
    }
protected void DrawCustomText(string textString, int fontSize, TextAnchor anchor, PoppinsStyle fontStyle, int uniformMargin)
    {
        GUIStyle temp = new GUIStyle(EditorStyles.label);
        temp.fontSize = fontSize;
        temp.alignment = anchor;
        temp.font = GetPoppinsFont(fontStyle);
        temp.padding = UniformRectOffset(uniformMargin);
        GUILayout.Label(textString, temp);
    }
    #endregion

    #region Styles
    protected static GUIStyle customTextStyle;
    protected static GUIStyle resetAllButtonStyle;
    protected static GUIStyle inputFieldStyle;
    protected static GUIStyle titleLabelStyle;
    protected static GUIStyle resultPicStyle;
    protected static GUIStyle resultLabelStyle;
    protected static GUIStyle iconStyle;
    protected static GUIStyle centeredLabelStyle;
    protected static GUIStyle defaultButtonStyle;
    protected static GUIStyle activeButtonStyle;
    protected static GUIStyle resetButtonStyle;
    protected static GUIStyle micButtonStyle;
    protected static GUIStyle smallButtonStyle;
    protected static GUIStyle smallButtonStyleDeactivated;
    protected static GUIStyle zoomBackButton;
    protected static GUIStyle dropDownStyle;
    protected static GUIStyle toggleStyle;
    protected static GUIStyle searchButtonStyle;
    protected static bool stylesInitialized = false;
    private static void InitializeCustomStyles()
    {
        if (stylesInitialized==false && texturesInitialized==true && fontInitialized==true)
        {
            
            try
            {
                int margin = 0;
                int padding = 0;
                #region Reset All Button Style
                resetAllButtonStyle = new GUIStyle(GUIStyle.none);
                int yDiff = (creatorButton.height / 2) - clearIcon.height;
                resetAllButtonStyle.margin.top = yDiff + 15;
                resetAllButtonStyle.margin.bottom = yDiff + 15;
                resetAllButtonStyle.margin.right = 10;
                resetAllButtonStyle.margin = new RectOffset(0, 10, yDiff + 15, yDiff + 15);
                #endregion

                #region Input Field Style
                inputFieldStyle = new GUIStyle(EditorStyles.textField);
                inputFieldStyle.font = POPPINS_MEDIUM;
                inputFieldStyle.fontSize = 14;
                //Margin
                margin = 10;
                inputFieldStyle.margin = UniformRectOffset(margin);
                //Padding
                padding = 0;
                inputFieldStyle.padding.top = padding;
                inputFieldStyle.alignment = TextAnchor.MiddleLeft;

                #endregion

                #region Define Label Style
                titleLabelStyle = new GUIStyle(EditorStyles.boldLabel);
                titleLabelStyle.font = POPPINS_BOLD;
                titleLabelStyle.alignment = TextAnchor.MiddleCenter;
                titleLabelStyle.fontSize = 20;
                margin = 10;
                titleLabelStyle.margin = UniformRectOffset(margin);
                #endregion

                #region Define Result Style

                resultPicStyle = new GUIStyle(GUI.skin.GetStyle("Button"));
                resultPicStyle.alignment = TextAnchor.MiddleCenter;
                //Set Pic Width
                float resultWidth = 200f;
                resultPicStyle.fixedWidth = resultWidth;
                resultPicStyle.fixedHeight = resultWidth;
                //Set Margins
                margin = 10;
                resultPicStyle.margin = UniformRectOffset(margin);
                //resultPicStyle.padding = UniformRectOffset(margin);
                #endregion

                #region Define Result Label Style
                resultLabelStyle = new GUIStyle(EditorStyles.label);
                resultLabelStyle.alignment = TextAnchor.MiddleLeft;
                margin = 0;
                resultLabelStyle.font = POPPINS_REGULAR;
                resultPicStyle.margin = UniformRectOffset(margin);
                #endregion

                #region IconStyle
                iconStyle = new GUIStyle(EditorStyles.label);

                iconStyle.normal.background = globeIcon;
                margin = 0;
                iconStyle.margin = UniformRectOffset(margin);
                padding = 0;
                iconStyle.padding = UniformRectOffset(padding);
                #endregion

                #region CentredLabel
                centeredLabelStyle = GUI.skin.GetStyle("Label");
                centeredLabelStyle.alignment = TextAnchor.MiddleLeft;

                #endregion

                #region Nav Button

                #region Nav Button Default
                defaultButtonStyle = new GUIStyle(EditorStyles.miniButton);
                defaultButtonStyle.fixedHeight = 50;
                defaultButtonStyle.font = POPPINS_BOLD;
                defaultButtonStyle.fontSize = 12;
                defaultButtonStyle.hover.textColor = GREEN_COLOR;
                margin = 10;
                defaultButtonStyle.margin = new RectOffset(margin, margin, margin, margin);

                #endregion

                #region Nav Button Active
                activeButtonStyle = new GUIStyle(EditorStyles.miniButton);
                activeButtonStyle.fixedHeight = 50;
                activeButtonStyle.font = POPPINS_BOLD;
                activeButtonStyle.fontSize = 12;
                activeButtonStyle.normal.textColor = BLUE_COLOR;
                activeButtonStyle.hover.textColor = BLUE_COLOR;
                margin = 10;
                activeButtonStyle.margin = new RectOffset(margin, margin, margin, margin);
                #endregion
                #endregion

                #region Search Button

                searchButtonStyle = new GUIStyle(EditorStyles.miniButton);
                searchButtonStyle.font = POPPINS_BOLD;
                searchButtonStyle.fixedHeight = 30;
                margin = 10;
                searchButtonStyle.margin = new RectOffset(margin, margin, margin, margin);
                #endregion

                #region Reset Button Style
                resetButtonStyle = new GUIStyle(EditorStyles.miniButton);
                resetButtonStyle.fixedHeight = 50;
                resetButtonStyle.font = POPPINS_REGULAR;
                resetButtonStyle.fontSize = 12;
                resetButtonStyle.normal.textColor = GREEN_COLOR;
                margin = 10;
                resetButtonStyle.margin = new RectOffset(margin, margin, margin, margin);
                #endregion

                #region Mic Button Style
                micButtonStyle = new GUIStyle(GUIStyle.none);
                micButtonStyle.alignment = TextAnchor.MiddleCenter;
                margin = 5;
                micButtonStyle.margin = UniformRectOffset(5);
                #endregion

                #region Small Button Style
                smallButtonStyle = new GUIStyle(EditorStyles.miniButton);
                //smallButtonStyle.fixedHeight = 50;
                smallButtonStyle.fixedWidth = 70;
                smallButtonStyle.fixedHeight = 30;
                smallButtonStyle.font = POPPINS_REGULAR;
                smallButtonStyle.fontSize = 12;
                //smallButtonStyle.normal.textColor = BLUE_COLOR;
                smallButtonStyle.hover.textColor = BLUE_COLOR;
                margin = 5;
                smallButtonStyle.margin = UniformRectOffset(margin);
                padding = 0;
                smallButtonStyle.padding = UniformRectOffset(padding);
                #endregion

                #region Small Button StyleDeactivated
                smallButtonStyleDeactivated = smallButtonStyle;
                smallButtonStyle.hover.textColor = Color.grey;
                smallButtonStyle.normal.textColor = Color.grey;
                smallButtonStyle.active.textColor = Color.grey;
                #endregion

                #region Zoom Back Button
                zoomBackButton = new GUIStyle(GUIStyle.none);
                padding = 5;
                zoomBackButton.padding = UniformRectOffset(padding);
                zoomBackButton.alignment = TextAnchor.MiddleCenter;
                #endregion

                #region Drop Down Menu
                dropDownStyle = new GUIStyle(EditorStyles.popup);
                //dropDownStyle.border = UniformRectOffset(20);
                dropDownStyle.stretchHeight = true;
                dropDownStyle.fixedHeight = 50;
                dropDownStyle.font = POPPINS_BOLD;
                dropDownStyle.fontSize = 12;
                dropDownStyle.hover.textColor = GREEN_COLOR;
                margin = 10;
                dropDownStyle.margin = UniformRectOffset(margin);
                padding = 10;
                dropDownStyle.padding = UniformRectOffset(padding);
                #endregion

                #region ToggleStyle
                toggleStyle = new GUIStyle(EditorStyles.toggle);
                toggleStyle.margin = smallButtonStyle.margin;
                toggleStyle.alignment = TextAnchor.MiddleLeft;
                toggleStyle.font = POPPINS_REGULAR;
                toggleStyle.fixedHeight = smallButtonStyle.fixedHeight;
                #endregion
            }
            catch(Exception e)
            {
                if (AnythingSettings.Instance.showDebugMessages)
                {
                    Debug.Log("Error initializing custom styles with error: ");
                    Debug.LogException(e);
                }
               return;
            }
            //Debug.Log("Styles initialized!");
            stylesInitialized = true;
        }
    }
    #endregion

    #region Textures
    protected static Texture2D logo;
    protected static Texture2D reset;
    protected static Texture2D globeIcon;
    protected static Texture2D clearIcon;
    protected static Texture2D creatorIcon;
    protected static Texture2D greenMicIcon;
    protected static Texture2D redMicIcon;
    protected static Texture2D greyMicIcon;
    protected static Texture2D micButtonTex;
    protected static Texture2D soundWavesGreen;
    protected static Texture2D soundWavesGrey;
    protected static Texture2D soundWavesOrange;
    #region Buttons
    protected static Texture2D creatorButton;
    protected static Texture2D voiceButton;
    protected static Texture2D habitatButton;
    protected static Texture2D signupButton;
    protected static Texture2D loginButton;
    #endregion
    #region Banners
    protected static Texture2D AWBanner;
    #endregion
    protected static Texture2D placeholderTexture;
    protected static Texture2D speechBubble;
    protected static Texture2D backgroundBlur;
    protected static Texture2D backButton;
    protected static Texture2D leftArrow;
    protected static Texture2D rightArrow;

    protected static bool texturesInitialized = false;
    private static void InitializeTextures()
    {
        if (!texturesInitialized)
        {
            //This is not optimal but is apparently the only way to solve an issue with call order of texture loading with Unity.
            //Will look back at it at later date.
            try
            {
                #region Headers
                AWBanner = Resources.Load("Banners/AWBanner") as Texture2D;
                logo = Resources.Load("Textures/Editor/logo_legless_white", typeof(Texture2D)) as Texture2D;
                reset = Resources.Load("Textures/Editor/back", typeof(Texture2D)) as Texture2D;

                globeIcon = Resources.Load("Textures/Editor/globe_arms") as Texture2D;
                clearIcon = Resources.Load("Textures/Editor/clear_all") as Texture2D;
                creatorIcon = Resources.Load("Textures/Editor/aw_creator") as Texture2D;
                #endregion

                #region Audio Panel
                greenMicIcon = Resources.Load("Textures/Editor/green_mic") as Texture2D;
                redMicIcon = Resources.Load("Textures/Editor/orange_mic") as Texture2D;
                greyMicIcon = Resources.Load("Buttons/miclightgrey") as Texture2D;
                soundWavesGreen = Resources.Load("Buttons/orangesound") as Texture2D;
                soundWavesOrange = Resources.Load("Buttons/orangesound") as Texture2D;
                soundWavesGrey = Resources.Load("Textures/Editor/soundwaves_grey_small") as Texture2D;
                #endregion


                #region Buttons
                creatorButton = Resources.Load("Buttons/CreatorButton") as Texture2D;
                voiceButton = Resources.Load("Buttons/VoiceButton") as Texture2D;
                habitatButton = Resources.Load("Buttons/HabitatButton") as Texture2D;
                loginButton = Resources.Load("Buttons/LoginButton") as Texture2D;
                signupButton = Resources.Load("Buttons/SignupButton") as Texture2D;
                #endregion

                #region Creator Panel
                placeholderTexture = Resources.Load("Textures/Editor/logo_legless_white") as Texture2D;
                speechBubble = Resources.Load("Textures/Editor/speech_bubble") as Texture2D;
                backgroundBlur = Resources.Load("Textures/Editor/background_opaque") as Texture2D;
                backButton = Resources.Load("Textures/Editor/back") as Texture2D;
                leftArrow = Resources.Load("Textures/Editor/leftArrow") as Texture2D;
                rightArrow = Resources.Load("Textures/Editor/rightArrow") as Texture2D;
                #endregion


            }
            catch (Exception e)
            {
                Debug.LogWarning("Textures failed to initialize with error: ");
                Debug.LogException(e);
                return;
            }

            texturesInitialized = true;
            //Debug.Log("Textures initialized");
        }

    }
    #endregion

    #region Fonts
    public static Font POPPINS_REGULAR;
    public static Font POPPINS_MEDIUM;
    public static Font POPPINS_BOLD;
    public static bool fontInitialized = false;
    private static void InitializeFonts()
    {
        if (!fontInitialized)
        {
            try
            {
                POPPINS_REGULAR = (Font)Resources.Load("Fonts/Poppins/Poppins-Regular", typeof(Font));
                POPPINS_MEDIUM = (Font)Resources.Load("Fonts/Poppins/Poppins-Medium", typeof(Font));
                POPPINS_BOLD = (Font)Resources.Load("Fonts/Poppins/Poppins-Bold", typeof(Font));
            }
            catch(Exception e)
            {
                Debug.LogWarning("Fonts failed to initialize with error: ");
                Debug.Log(e);
                return;
            }



            fontInitialized = true;
            //Debug.Log("Fonts initialized");
        }
    }
    #endregion

    #region Utility Functions

    Font GetPoppinsFont(PoppinsStyle style)
    {
        Font chosenFont = null;
        switch (style)
        {
            case PoppinsStyle.Regular:
                chosenFont = POPPINS_REGULAR;
                break;
            case PoppinsStyle.Medium:
                chosenFont = POPPINS_MEDIUM;
                break;
            case PoppinsStyle.Bold:
                chosenFont = POPPINS_BOLD;
                break;
            default:
                chosenFont = POPPINS_REGULAR;
                break;

        }
        return chosenFont;
    }
    protected static RectOffset UniformRectOffset(int offset)
    {
        return new RectOffset(offset, offset, offset, offset);
    }
    protected void ToggleGUIClear()
    {
        if (GUI.color != Color.clear)
        {
            guiColor = GUI.color;
            GUI.color = Color.clear;
        }
        else
        {
            GUI.color = guiColor;
        }
    }



    protected void ToggleGUIColor(Color tempColor)
    {
        if (GUI.color != tempColor)
        {
            guiColor = GUI.color;
            GUI.color = tempColor;
        }
        else
        {
            GUI.color = guiColor;
        }
    }
    protected void ToggleGUIBackgroundColor(Color tempColor)
    {
        if (GUI.backgroundColor != tempColor)
        {
            guiColor = GUI.backgroundColor;
            GUI.backgroundColor = tempColor;
        }
        else
        {
            GUI.backgroundColor = guiColor;
        }
    }


    protected void ToggleGUIBackgroundClear()
    {
        if (GUI.backgroundColor != Color.clear)
        {
            guiColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.clear;
        }
        else
        {
            GUI.backgroundColor = guiColor;
        }
    }

    public static void SpaceWrapped(float pixels)
    {
        try
        {
            GUILayout.Space(pixels);
        }
        catch
        {
            GUIUtility.ExitGUI();
        }
    }
    public static void FlexibleSpaceWrapped()
    {
        try
        {
            GUILayout.FlexibleSpace();
        }
        catch
        {
            GUIUtility.ExitGUI();
        }
    }
    #endregion


}