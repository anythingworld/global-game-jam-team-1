﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
public class AnythingEditorScenePanel : AnythingEditor
{
    #region Fields
    private float panelWidth;
    private float panelHeight;
    SerializedProperty serializedProp;

    #region Row Properties
    private int instancesInScene = 0;
    private int row = 0;

    [SerializeField]
    private List<DrawerBoolProp> rowBoolProps = null;

    private readonly string[] toolbarOptionStrings = { "Dynamic", "Habitat" };
    #endregion

    #endregion

    public void OnHierarchyChange()
    {
        UpdateSceneLedger();
    }

    private void UpdateSceneLedger()
    {
        foreach (var group in SceneLedger.ThingGroups.ToArray())
        {
            for (int i = 0; i < group.objInstances.Count; i++)
            {
                if (group.objInstances[i] == null /*|| group.awInstances[i] == null*/)
                {
#if UNITY_EDITOR
                    SceneLedger.ThingGroups.Remove(group);
#endif
                }
            }
        }
    }



    private void UpdateGUIVariables(ThingGroup[] groupArray)
    {
        //If not initialised, initialise GUI variable lists for display drawer
        if (rowBoolProps == null)
        {
            //scrollPos = new List<Vector2>();
            rowBoolProps = new List<DrawerBoolProp>();
            foreach (var group in groupArray)
            {

                DrawerBoolProp newBoolProp = new DrawerBoolProp();
                newBoolProp.groupName = group.objectName;
                newBoolProp.drawerActive = false;
            }

        }
        else if (groupArray.Length != rowBoolProps.Count)
        {
            List<DrawerBoolProp> newBoolProp = new List<DrawerBoolProp>();
            foreach (var group in groupArray)
            {
                bool found = false;
                DrawerBoolProp temp = new DrawerBoolProp();
                foreach (var prop in rowBoolProps.ToArray())
                {
                    if (prop.groupName == group.objectName)
                    {
                        temp = prop;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    temp.groupName = group.objectName;
                    temp.drawerActive = false;

                }

                newBoolProp.Add(temp);
            }

            rowBoolProps = newBoolProp;
        }
    }

    private Vector2 scrollPos;
    List<ThingGroup> thingGroupList = null;


    public void OnGUI()
    {
        #region Initialisations
        if (thingGroupList == null)
        {
            thingGroupList = new List<ThingGroup>();
        }
        if (Event.current.type == EventType.Repaint)
        {
            Setup();
            GetWindowSize();

            if (thingGroupList == null)
            {
                thingGroupList = SceneLedger.ThingGroups;
            }


        }
        InitializeResources();
        #endregion



        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        DrawHeader();
        GUILayout.EndHorizontal();
        DrawUILine(Color.grey);



        #region Draw Creature Rows

        scrollPos = GUILayout.BeginScrollView(scrollPos);

        row = 0;

        if (Event.current.type == EventType.Repaint )
        {

            thingGroupList = SceneLedger.ThingGroups;
            UpdateGUIVariables(thingGroupList.ToArray());

        }

        DrawRows(thingGroupList.ToArray());
        GUILayout.EndScrollView();
        #endregion


        GUILayout.EndVertical();
    }

    /// <summary>
    /// Draw rows for each <see cref="ThingGroup"/> category in the scene.
    /// </summary>
    /// <param name="_thingGroupArray">Array holding <see cref="th"/> objects. </param>
    private void DrawRows(ThingGroup[] _thingGroupArray)
    {
        try
        {
            foreach (ThingGroup group in _thingGroupArray)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Space(5);
                GUILayout.BeginVertical();
                GUILayout.Space(5);
                DrawRow(group);
                GUILayout.Space(5);
                GUILayout.EndVertical();
                GUILayout.Space(5);
                GUILayout.EndHorizontal();
            }
        }
        catch
        {
            //Debug.Log(Event.current.type.ToString());
            // Debug.Log(thingGroupArray.Length);
        }
    }

    /// <summary>
    /// Open window when user selects panel in menu bar.
    /// </summary>
    [MenuItem("Anything World/Anything Scene panel",false,2)]
    public static void Init()
    {
        UnityEditor.EditorWindow window = GetWindow(typeof(AnythingEditorScenePanel), false, "Anything Scene");
        window.position = new Rect(100, 100, 250, 100);
        window.Show();
    }


    #region Draw Elements
    private void DrawHeader()
    {
        DrawBoldTextHeader("Scene View");
    }

    public int tempGoal = 1;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="group"><see cref="ThingGroup"/> displayed in row.</param>
    private void DrawRow(ThingGroup group)
    {

        GameObject[] objInstances = group.objInstances.ToArray();
        AWObj[] awInstances = group.awInstances.ToArray();


        GUILayout.BeginVertical();
        instancesInScene = objInstances.Length;
        GUILayout.BeginHorizontal();
        DrawCustomText(Creator.GetDisplayName(group.objectName) + " (" + group.objectName + ")", 16, TextAnchor.UpperLeft, PoppinsStyle.Regular);

        if (GUILayout.Button(clearIcon, GUIStyle.none, GUILayout.ExpandWidth(false)))
        {
            SceneLedger.RemoveGroup(group);
            Creator.ResetAutoLayout(true);
        }
        GUILayout.EndHorizontal();


        #region Display Clones
        GUILayout.BeginHorizontal();
        DrawCustomText("Clones: "+instancesInScene, 12, TextAnchor.UpperLeft, PoppinsStyle.Regular);
      
        group.HeldGoalInstances = (int)GUILayout.HorizontalSlider(group.HeldGoalInstances, 1, 20);

        if (Event.current.type == EventType.Repaint && group.GoalInstances != group.HeldGoalInstances)
        {
            group.GoalInstances = group.HeldGoalInstances;
        }
        

        GUILayout.EndHorizontal();
        #endregion

        #region GO and AWObj Foldout
        GUILayout.BeginVertical();

        if (row >= rowBoolProps.Count)
        {
            EditorGUILayout.EndFoldoutHeaderGroup();
            GUILayout.EndVertical();
            GUILayout.EndVertical();
            return;
        }
        rowBoolProps[row].drawerActive = EditorGUILayout.BeginFoldoutHeaderGroup(rowBoolProps[row].drawerActive, "Show Game Objects");


        if (rowBoolProps[row].drawerActive)
        {
            for (int i = 0; i < objInstances.Length; i++)
            {
                GUILayout.BeginHorizontal();
                bool objField = (GameObject)EditorGUILayout.ObjectField("GameObject", objInstances[i], typeof(GameObject), false);
                bool awobjField = (AWObj)EditorGUILayout.ObjectField("AWObj", awInstances[i], typeof(AWObj), false);
                GUILayout.EndHorizontal();
            }

        }
        DrawUILine(Color.grey);
        EditorGUILayout.EndFoldoutHeaderGroup();
        GUILayout.EndVertical();
        #endregion

        GUILayout.EndVertical();
        row++;
    }
    #endregion

    #region Utilities
    private void GetWindowSize()
    {

        GetWindow<AnythingEditorScenePanel>("position", false);
        Rect r = position;
        panelWidth = position.width;
        panelHeight = position.height;
    }
    private void GetSceneItems()
    {

    }
    #endregion
}

[Serializable]
public class DrawerBoolProp
{
    [SerializeField]
    public string groupName;
    [SerializeField]
    public bool drawerActive = false;

    public void SetGroup(string group)
    {
        groupName = group;
    }

    public void SetGroupActive(bool active)
    {
        drawerActive = active;
    }

}