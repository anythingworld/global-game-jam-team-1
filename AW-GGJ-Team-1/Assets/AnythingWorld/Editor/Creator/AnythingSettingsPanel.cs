﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AnythingSettingsPanel : AnythingEditor
{

    #region Fields
    private static string awKey;
    private static string appName;
    private static string agentID;
    private static string email;
    #endregion
    //[InitializeOnLoadMethod]
    public static void Init()
    {
    }

    [MenuItem("Anything World/Settings",false,21)]
    public static void MenuLoad()
    {
        Init();
        ShowWindow();
    }

    public static void ShowWindow()
    {
        awKey = Creator.AWSettings.APIKey;
        appName = Creator.AWSettings.AppName;
        agentID = Creator.AWSettings.AgentId;
        email = Creator.AWSettings.Email;

        AnythingSettingsPanel window = ScriptableObject.CreateInstance(typeof(AnythingSettingsPanel)) as AnythingSettingsPanel;
        //AnythingAPIGen window = EditorWindow.GetWindow<AnythingAPIGen>();
        GUIContent windowContent = new GUIContent("AnythingWorld Settings");
        window.titleContent = windowContent;
        window.ShowUtility();

    }
    public static void CloseWindow()
    {
        if (HasOpenInstances<AnythingSettingsPanel>())
        {
            AnythingSettingsPanel window = GetWindow<AnythingSettingsPanel>() as AnythingSettingsPanel;
            window.Close();
        }

    }
    private void OnGUI()
    {

        InitializeResources();

        SettingsWindow();
        


    }

    private void SettingsWindow()
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        GUILayout.BeginVertical();

        //int spacer = 5;
        //int bigspacer = 10;
        int textSize = 14;
        //DrawUILine(Color.gray);
        DrawBoldTextHeader("Settings");
        GUIStyle fieldStyle = new GUIStyle(EditorStyles.textField);
        fieldStyle.font = POPPINS_REGULAR;
        fieldStyle.fixedWidth = 250;

        GUILayout.BeginHorizontal();
        DrawCustomText("API Key ", textSize);
        awKey = GUILayout.TextField(awKey, fieldStyle);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        DrawCustomText("App Name ", textSize);
        appName = GUILayout.TextField(appName, fieldStyle);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        DrawCustomText("Agent ID ", textSize);
        agentID = GUILayout.TextField(agentID, fieldStyle);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        DrawCustomText("Email ", textSize);
        email = GUILayout.TextField(email, fieldStyle);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Apply", searchButtonStyle))
        {
            Debug.Log(Creator.AWSettings.APIKey);
            Creator.AWSettings.APIKey = awKey;
            if (String.IsNullOrEmpty(appName)) appName = "My App";
            Creator.AWSettings.AppName = appName;
            Creator.AWSettings.AgentId = agentID;
            Creator.AWSettings.Email = email;
            CloseWindow();
        }
        if (GUILayout.Button("Reset", searchButtonStyle))
        {
            Init();
        }
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        GUILayout.Space(10);
        GUILayout.EndHorizontal();
        
    }
}
