﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// Custom editor for the RandomMovement behaviour.
/// </summary>
[CustomEditor(typeof(RandomMovement))]
public class RandomMovementEditor : Editor
{
    #region Fields
    private RandomMovement controller;
    private float size = 1f;
    #endregion

    #region Unity Callbacks
    private void OnEnable()
    {
        controller = (RandomMovement)target;
    }
    private void OnSceneGUI()
    {
        if (Event.current.type == EventType.Repaint)
        {
            Transform transform = ((RandomMovement)target).transform;
            Vector3 targetPos = controller.TargetPosition;
            Handles.color = new Color(0.5f, 0f, 0.5f, 0.5f);
            Handles.DrawSolidDisc(targetPos, new Vector3(0, 1, 0), controller.TargetRadius);
            Handles.color = Handles.xAxisColor;
            Handles.SphereHandleCap(0, targetPos, transform.rotation, size, EventType.Repaint);

        }
    }
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        if (GUILayout.Button("Refresh Animator"))
        {
            controller.AddAWAnimator();
        }
    }
    #endregion

}
