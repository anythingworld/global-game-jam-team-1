﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AnythingObject))]
public class AnythingObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        AnythingObject anythingObject = (AnythingObject)target;
        if (GUILayout.Button("Refresh"))
        {
            anythingObject.CreateObject();
        }
    }
}
