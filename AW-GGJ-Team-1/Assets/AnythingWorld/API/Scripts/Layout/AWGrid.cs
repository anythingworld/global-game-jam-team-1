using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;




[Serializable]
public class AWGrid
{
    private const bool SHOW_DEBUG_STATUS = true;
    private const int EMPTY_VALUE = 0;
    private const int FULL_VALUE = 1;
    #region Grid Size Props
    [SerializeField]
    private int width;
    public int Width => width;
    [SerializeField]
    private int depth;
    public int Depth => depth;
    [SerializeField]
    private float cellSize;
    public float Cellsize => cellSize;
    #endregion
    private int[,] gridArray = null;
    public int[,] GridArray
    {
        get
        {
            if (gridArray == null)
            {
                if (DeSerializeGridArray())
                {
                    return gridArray;
                }
                else
                {
                    if (gridArray == null)
                    {
                        InitNewGrid();
                        return gridArray;
                    }
                    return null;
                }
            }
            else
            {
                return gridArray;
            }
        }
    }


    #region Creation Props
    [SerializeField]
    private List<GridInstruction> instructionList;
    [SerializeField]
    private Vector3 startPos = new Vector3(-45, 0, -45);
    [SerializeField]
    byte[] stream;
    [SerializeField]
    private int creationWidth;
    [SerializeField]
    private int creationDepth;
    #endregion

    [SerializeField]
    private bool _noSpaceLeft = false;
    public bool NoSpaceLeft => _noSpaceLeft;

    public AWGrid(int gWidth, int gDepth, float gCellSize = 10f)
    {
        //Debug.Log("New Grid being made");
        width = creationWidth = gWidth;
        depth = creationDepth = gDepth;
        cellSize = gCellSize;
        gridArray = new int[width * 100, depth * 100]; // TODO: stop cheating here to allow for expansion of grid - GM
        instructionList = new List<GridInstruction>();

        for (int i = 0; i < depth; i++)
        {
            for (int j = 0; j < width; j++)
            {
                gridArray[i, j] = EMPTY_VALUE;

                if (j < width - 1)
                {
                    Debug.DrawLine(GetWorldPosition(i, j), GetWorldPosition(i, j + 1), Color.magenta, 1f);
                }
                if (i < depth - 1)
                {
                    Debug.DrawLine(GetWorldPosition(i, j), GetWorldPosition(i + 1, j), Color.magenta, 1f);
                }
            }
        }
#if UNITY_EDITOR
        // Debug.Log("add serialization listener");
        EditorApplication.playModeStateChanged += SerializeChanges;
#endif
    }

    public void InitNewGrid()
    {
        gridArray = new int[width * 100, depth * 100];
        try
        {
            AnythingCreator.Instance.ResetAutoLayout(true);
        }
        catch
        {
            Debug.LogError("No anything creator instantiated, no autolayout reset.");
        }
    }


    private void DrawDebugStatus()
    {
        float lineWidth = 1f;
        for (int y = 0; y < depth; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Color gridColor = Color.red;
                if (gridArray[y, x] == 0)
                    gridColor = Color.white;

                Debug.DrawLine(GetWorldPosition(y, x), GetWorldPosition(y, x + 1), gridColor, lineWidth, false);
                Debug.DrawLine(GetWorldPosition(y, x), GetWorldPosition(y + 1, x), gridColor, lineWidth, false);


                if (x == width - 1)
                {
                    Debug.DrawLine(GetWorldPosition(y, x + 1), GetWorldPosition(y + 1, x + 1), gridColor, lineWidth, false);
                }
                if (y == depth - 1)
                {
                    Debug.DrawLine(GetWorldPosition(y + 1, x), GetWorldPosition(y + 1, x + 1), gridColor, lineWidth, false);
                }
            }


        }
    }

    public void ExpandGrid(int extraWidth, int extraDepth)
    {
        width += extraWidth;
        depth += extraDepth;
    }

    private Vector3 GetWorldPosition(float z, float x)
    {
        Vector3 worldPos = new Vector3(x, 0, z) * cellSize;
        var temp = worldPos + startPos;
        return temp;
    }

    public void SetValueRange(int xStart, int zStart, int xRange, int zRange, int value)
    {
        if (gridArray == null)
            DeSerializeGridArray();

        for (int i = zStart; i < zStart + zRange; i++)
        {
            for (int j = xStart; j < xStart + xRange; j++)
            {
                gridArray[i, j] = value;
            }
        }

        if (value != EMPTY_VALUE)
        {
            GridInstruction newInstruction = new GridInstruction(xStart, zStart, xRange, zRange);
            instructionList.Add(newInstruction);

            // Debug.Log("added to instructionList = " + instructionList.Count);
        }

        if (SHOW_DEBUG_STATUS)
            DrawDebugStatus();
    }

    public void RemoveLastInstruction()
    {

        // Debug.Log("removed from instructionList = " + instructionList.Count);

        int removeIndex = instructionList.Count - 1;
        if (removeIndex < 0)
        {
            Debug.LogError("RemoveLastInstruction index error!");
            return;
        }

        GridInstruction lastInstruction = instructionList[removeIndex];
        SetValueRange(lastInstruction.startX, lastInstruction.startZ, lastInstruction.rangeX, lastInstruction.rangeZ, EMPTY_VALUE);
        instructionList.RemoveAt(removeIndex);

        if (SHOW_DEBUG_STATUS)
            DrawDebugStatus();

        if (instructionList.Count < creationWidth * creationDepth)
        {
            Debug.Log("return to normal grid!");
            width = creationWidth;
            depth = creationDepth;
        }
    }

    public Vector3 GetNextAvailablePos(float objScale)
    {
        //Debug.Log("Getting next available pos");
        if (GridArray == null)
        {
            Debug.LogWarning("No grid detected, placing at origin");
            return Vector3.zero;
        }


        _noSpaceLeft = false;
        int objSquares = Mathf.CeilToInt(objScale);
        // Debug.Log("object squares = " + objSquares);
        for (int i = 0; i < depth; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (gridArray[i, j] == EMPTY_VALUE)
                {
                    int sqCount = 0;
                    bool haveSpace = true;
                    while (sqCount < objSquares)
                    {
                        int targSquare = j + sqCount;
                        if (targSquare < width)
                        {
                            if (gridArray[i, targSquare] > EMPTY_VALUE)
                            {
                                haveSpace = false;
                            }
                        }
                        else
                        {
                            haveSpace = false;
                        }
                        sqCount++;
                    }

                    if (haveSpace)
                    {
                        SetValueRange(j, i, sqCount, sqCount, FULL_VALUE);

                        float sqCenter = sqCount / 2f;
                        Vector3 availPos = GetWorldPosition(i + sqCenter, j + sqCenter);
                        // Debug.Log("availPos = " + availPos);
                        return availPos;
                    }
                }
            }
        }
        _noSpaceLeft = true;
        return Vector3.zero;
    }
#if UNITY_EDITOR
    private void SerializeChanges(PlayModeStateChange state)
    {
        // Debug.Log("SerializeChanges : " + state);

        // this state change never gets reported, which seems a little silly of Unity - GM
        if (state == PlayModeStateChange.EnteredEditMode)
        {
            DeSerializeGridArray();
        }
        else if (state == PlayModeStateChange.ExitingEditMode)
        {
            SerializeGridArray();
        }
    }
#endif

    private bool SerializeGridArray()
    {
        if (gridArray != null)
        {
            stream = AWUtils.ObjectSerializationExtension.SerializeToByteArray(gridArray);
            return true;
        }
        else
        {
            Debug.LogWarning("Grid array is null, cannot serialize to byte array.");
            return false;
        }
    }

    private bool DeSerializeGridArray()
    {
        if (stream != null)
        {
            if (stream.Length > 0)
            {
                gridArray = AWUtils.ObjectSerializationExtension.Deserialize<int[,]>(stream);
                return true;
            }
            else
            {
                //Debug.LogWarning("Attempted to deserialize empty byte stream.");
                return false;
            }
        }
        else
        {
            Debug.LogWarning("Byte stream null, cannot be deserialized");
            return false;
        }
    }
}