﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnythingObjectRandom : AnythingObject
{
    public bool RandomiseWhenVisible = false;
    public bool MakeOnStart = true;

    private List<string> _objectList;

    private bool _objectListReady = false;

    protected new void Start()
    {
        // anythingCreator = AnythingCreator.Instance;
        _objectList = new List<string>();
        StartCoroutine(WaitForList());
    }

    private IEnumerator WaitForList()
    {
        yield return new WaitForSeconds(0.1f);

        while (!AnythingObjectRandomList.Instance.RandomListLoaded)
            yield return new WaitForSeconds(0.1f);

        _objectList = AnythingObjectRandomList.Instance.Creatures;
        _objectListReady = true;

        if (MakeOnStart)
        {
            MakeRandomObject();
        }
    }

    private IEnumerator OnBecameVisible()
    {
        if (RandomiseWhenVisible)
        {
            while (!_objectListReady)
                yield return new WaitForEndOfFrame();

            MakeRandomObject();
        }
    }

    private void OnBecameInvisible()
    {
        if (RandomiseWhenVisible)
        {
            Transform firstTransform = GetComponent<Transform>();

            if (firstTransform.childCount > 0)
                Destroy (firstTransform.GetChild (0).gameObject);
        }
    }

    private void MakeRandomObject()
    {
        int itemIndex = Random.Range(0,_objectList.Count-1);

        ObjectName = _objectList[itemIndex];

        Debug.Log("Random Object = " + ObjectName);

        CreateObject();
    }
}
