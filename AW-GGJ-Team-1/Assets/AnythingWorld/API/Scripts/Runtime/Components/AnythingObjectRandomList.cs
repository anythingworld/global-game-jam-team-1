﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AnythingObjectRandomList : MonoBehaviour
{
    private const string API_OBJECTS = "https://anything-world-api.appspot.com/names";
    private List<string> _creatures;
    public List<string> Creatures
    {
        get
        {
            return _creatures;
        }
    }
    private bool _randomListLoaded = false;
    public bool RandomListLoaded 
    {
        get
        {
            return _randomListLoaded;
        }
    }

    private static AnythingObjectRandomList instance;

    public static AnythingObjectRandomList Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject AnythingObjectRandomListGO = new GameObject();
                AnythingObjectRandomListGO.name = "Anything Object Random List";
                AnythingObjectRandomList anythingObjectRandomList = AnythingObjectRandomListGO.AddComponent<AnythingObjectRandomList>();
                instance = anythingObjectRandomList;
            }
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        StartCoroutine(LoadCreatures());
    }

    private IEnumerator LoadCreatures()
    {
        

        UnityWebRequest www = UnityWebRequest.Get(API_OBJECTS);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            //Debug.LogError("Error requesting things!");
            yield break;
        }

        string result = www.downloadHandler.text;
        result.Trim(new Char[] { ' ', ']', '.' });
        result = result.Replace("\"", "");
        _creatures = new List<string>(result.Split(','));
        _creatures = _creatures.ConvertAll(d => d.ToLower());

        // remove more than one word results
        for(int i=0;i<_creatures.Count;i++)
        {
            if (_creatures[i].IndexOf(" ") != -1)
            {
                _creatures.RemoveAt(i);
            }
        }

        _randomListLoaded = true;
        
    }

}