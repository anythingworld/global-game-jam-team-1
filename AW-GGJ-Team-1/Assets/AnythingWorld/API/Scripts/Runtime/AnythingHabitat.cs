﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnythingHabitat : AnythingBase
{
    #region Fields
    private AWHabitat _AWHabitat;
    private Dictionary<string, List<GameObject>> _madeThings;
    private static AnythingHabitat instance;
    public static AnythingHabitat Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject anythingCreatorGO = new GameObject();
                anythingCreatorGO.name = "Anything Habitat";
                AnythingHabitat anythingCreator = anythingCreatorGO.AddComponent<AnythingHabitat>();
                instance = anythingCreator;
            }
            return instance;
        }
    }
    #endregion

    #region Public Methods

    public void MakeHabitat(string habitatName, bool createCreatures)
    {
        StartCoroutine(MakeHabitatRoutine(habitatName, createCreatures));
    }
    public void MakeHabitat(string habitatName)
    {
        StartCoroutine(MakeHabitatRoutine(habitatName, false));
    }
    #endregion

    #region Private Methods
    private IEnumerator MakeHabitatRoutine(string habitatName, bool createCreatures)
    {
        // TODO: checkbox to confirm / option to not remove in editor
        ResetEverything();

        GameObject habitatCreator = new GameObject();
        habitatCreator.name = "Habitat Creator";
        _AWHabitat = habitatCreator.AddComponent<AWHabitat>();
        _AWHabitat.MakeHabitat(habitatName);

        while (!_AWHabitat.AWHabitatReady)
        {
            yield return new WaitForEndOfFrame();
        }

        Debug.Log($"_AWHabitat.RandomObjects = {_AWHabitat.RandomObjects}");

        Debug.Log($"createCreatures = {createCreatures}");

        if ((habitatName != "testing") && (createCreatures))
        {
            StartCoroutine(MakeHabitatObjects(_AWHabitat.RandomObjects));
        }
        else
        {
            AnythingSetup.Instance.ShowLoading(false);
        }
    }

    private IEnumerator MakeHabitatObjects(string[] habitatObjects)
    {
        AnythingCreator anythingCreator = AnythingCreator.Instance;
        foreach (string objString in habitatObjects)
        {
            anythingCreator.MakeObject(objString);
        }

        while (!anythingCreator.MakeObjectRoutineRunning)
            yield return new WaitForEndOfFrame();
    }
    #endregion
    // TODO: add enums / defined list here
}
