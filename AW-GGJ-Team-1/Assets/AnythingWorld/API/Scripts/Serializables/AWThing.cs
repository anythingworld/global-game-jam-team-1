﻿using System;

[Serializable]
public class AWThing
{
    public string habitat = "";
    public string _id;
    public string name;
    public string creature;
    public string group;
    public string shape;
    public string behaviour = "none";
    public bool pet;
    public string detail;
    public AWThingAbout model;
    public string author;
    // public string __v;
    public string mtl;
    public string source;
    public string type;
    public AWScale scale;
    public string reference;

}
