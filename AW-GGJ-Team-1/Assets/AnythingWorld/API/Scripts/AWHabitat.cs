using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
using Unity.EditorCoroutines.Editor;
#endif
using UnityEngine.Networking;

[ExecuteAlways]
// [System.Serializable]
public class AWHabitat : MonoBehaviour
{
    // TODO: environment loading for habitats
    private const string API_PREFIX = "https://anything-world-api.appspot.com/anything?";
    private const string API_SUFFIX = "&key=";
    private const string APP_SUFFIX = "&app=";
    private const int HABITAT_OBJECT_TOTAL = 25;
    private const int HABITAT_VARIETY_TOTAL = 5;

    private string _habitatKey;
    private AWThing[] _awThings;
    private List<string> _objectsCreated;
    private int _objectVariety;
    public bool _awObjNotFound = false;
    public bool _awKeyInvalid = false;
    public bool _awAppIdInvalid = false;
    private bool _awHabitatReady;
    public bool AWHabitatReady
    {
        get
        {
            return _awHabitatReady;
        }
    }

    private string[] _randomObjects;
    public string[] RandomObjects
    {
        get
        {
            return _randomObjects;
        }
    }
    public bool AWObjNotFound
    {
        get
        {
            return _awObjNotFound;
        }
        set
        {
            _awObjNotFound = value;
        }
    }
    public bool AWKeyInvalid
    {
        get
        {
            return _awKeyInvalid;
        }
        set
        {
            _awKeyInvalid = value;
        }
    }
    public bool AWAppIdInvalid
    {
        get
        {
            return _awAppIdInvalid;
        }
        set
        {
            _awAppIdInvalid = value;
        }
    }

    public void MakeHabitat(string habitatKey)
    {
        _awHabitatReady = false;
        _habitatKey = habitatKey;
        StartCoroutine(RequestHabitatObjects());
        _objectsCreated = new List<string>();
        _objectVariety = 0;
    }

    private IEnumerator RequestHabitatObjects()
    {

        if (String.IsNullOrEmpty(AnythingSettings.Instance.AppName))
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Please enter an App Name in AnythingSettings!");
            _awAppIdInvalid = true;
            yield break;
        }

        string APICall = API_PREFIX + _habitatKey + "=true" + API_SUFFIX + AnythingSettings.Instance.APIKey + APP_SUFFIX + AnythingSettings.Instance.AppName;

        UnityWebRequest www = UnityWebRequest.Get(APICall);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            AnythingSetup.Instance.ShowLoading(false);
            Debug.LogError("Network Error for Anything World using API Key: " + AnythingSettings.Instance.APIKey);
            _awKeyInvalid = true;
            yield break;
        }

        string result = www.downloadHandler.text;
        // no models in this habitat, just make environment
        if (result.IndexOf("no model") != -1)
        {
            MakeEnvironment();
            yield break;
        }

        //Debug.Log("habitat result: " + www.downloadHandler.text);

        _awThings = JSONHelper.GetJSONArray<AWThing>(www.downloadHandler.text);

        //Debug.Log(_awThings);
        int totalObjects = _awThings.Length;

        if (Application.isEditor && !Application.isPlaying)
        {
#if UNITY_EDITOR
            EditorCoroutineUtility.StartCoroutine(MakeRandomHabitatCreatures(), this);
#endif
        }
        else
        {
            StartCoroutine(MakeRandomHabitatCreatures());
        }

        // foreach(AWThing awThing in _awThings)
        // {
        //     Debug.Log(awThing.name);
        // }
    }

    private IEnumerator MakeRandomHabitatCreatures()
    {
        int createdObjects = 0;
        string randomObject;
        int randomIndex;
        _randomObjects = new string[HABITAT_OBJECT_TOTAL];
        while (createdObjects < HABITAT_OBJECT_TOTAL)
        {


            if (_objectVariety < HABITAT_VARIETY_TOTAL)
            {
                randomIndex = UnityEngine.Random.Range(0, _awThings.Length);
                randomObject = _awThings[randomIndex].name;
                if (!_objectsCreated.Contains(randomObject))
                {
                    _objectVariety++;
                    _objectsCreated.Add(randomObject);
                }
            }
            else
            {
                randomIndex = UnityEngine.Random.Range(0, _objectsCreated.Count);
                randomObject = _objectsCreated[randomIndex];
            }


            _randomObjects[createdObjects] = randomObject;

            createdObjects++;
        }
        _awHabitatReady = true;
        MakeEnvironment();
        yield return null;
    }

    private IEnumerator MakeAWObject(string objectToMake)
    {
        Debug.Log("runtime make object : " + objectToMake);
        AnythingCreator.Instance.MakeObject(objectToMake);
        yield return new WaitForEndOfFrame();
    }

    private void MakeEnvironment()
    {
        GameObject environmentObject = EnvironmentMap.EnvironmentPrefab(_habitatKey);
        GameObject environmentPrefab = Instantiate(environmentObject, environmentObject.transform.localPosition, environmentObject.transform.rotation) as GameObject;
        environmentPrefab.transform.parent = transform;

        // reset all AnythingObjects in environment
        AnythingObject[] anythingObjects = environmentPrefab.GetComponentsInChildren<AnythingObject>();
        foreach (AnythingObject anythingObject in anythingObjects)
        {
            anythingObject.ObjectCreated = false;
        }

        EnvironmentLights environLights = EnvironmentMap.EnvironmentLighting(_habitatKey);
        GameObject awLightGO = GameObject.FindGameObjectWithTag("AWSun");

        Material SkyBoxMaterial = Resources.Load("Materials/" + environLights.SkyBoxMaterial) as Material;
        RenderSettings.skybox = SkyBoxMaterial;

        //Debug.Log("change skybox to : " + environLights.SkyBoxMaterial);

        if (awLightGO != null)
        {
            Light awLight = awLightGO.GetComponent<Light>();
            awLight.color = environLights.LightColor;
            awLight.intensity = environLights.LightIntensity;
        }

        RenderSettings.fogColor = environLights.FogColor;
        RenderSettings.fogMode = environLights.FogType;
        if (environLights.FogType == FogMode.ExponentialSquared)
        {
            RenderSettings.fogDensity = environLights.FogDensity;
        }
        else if (environLights.FogType == FogMode.Linear)
        {
            RenderSettings.fogStartDistance = environLights.FogStart;
            RenderSettings.fogEndDistance = environLights.FogEnd;
            //Debug.Log($"environLights.FogEnd = {environLights.FogEnd}");
        }
    }

}