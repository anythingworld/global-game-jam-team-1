// this is the message format expected
// {
// 	"queryInput": {
//     "text": {
//       "text": "speak to a bat",
//       "languageCode": "en-US"
//     }
// 	}
// }
// }

using System;

[Serializable]
public class AWTextMessageInput
{
    public AWQueryInput queryInput;
    // public AWDFAgent agentId;

    public AWTextMessageInput(string message)
    {
        queryInput = new AWQueryInput();
        queryInput.text = new AWTextInput();
        queryInput.text.text = message;
        // agentId = new AWDFAgent();

    }
}