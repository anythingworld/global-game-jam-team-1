﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
/// <summary>
/// 
/// </summary>
public class AnythingChat
{
    #region Fields
    public const string API_LOCATION_BASE = "http://anything-world-api.appspot.com/df/";
    public const string API_LOCATION_TEXT = "textRequest";
    public const string API_LOCATION_AUDIO = "audioRequest";
    private AWNaturalLanguageResponse awResponse;
    public delegate void AWResponseHandler(AWNaturalLanguageResponse awResponse);
    public event AWResponseHandler OnAWResponse;

    private bool SHOW_DEBUG = true;



    #endregion

    #region Public Methods
    /// <summary>
    /// Get a conversational response to a string, powered by the DialogFlow chatbot.
    /// </summary>
    /// <param name="message">Input string to generate response from.</param>
    /// <param name="responseCallback">Reference to callback that will receive AWNaturalLanguageResponse response from DialogFlow.</param>
    /// <param name="routineObj">Reference to MonoBehaviour function owning the new coroutine.</param>
    public void Talk(string message, AWResponseHandler responseCallback, MonoBehaviour routineObj)
    {
        OnAWResponse += responseCallback;
        routineObj.StartCoroutine(SendText(message, awReply =>
        {
            OnAWResponse(awResponse);
            OnAWResponse -= responseCallback;
        }));
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="message"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    public IEnumerator SendText(string message, System.Action<AWNaturalLanguageResponse> callback)
    {

        string APICall = API_LOCATION_BASE + API_LOCATION_TEXT;
        AWTextMessageInput awMessage = new AWTextMessageInput(message);
        string postData = JsonUtility.ToJson(awMessage);
        if (SHOW_DEBUG) Debug.Log(postData);
        byte[] bytes = Encoding.UTF8.GetBytes(postData);

        // unity post bytes hack. my apologies. 
        UnityWebRequest www = UnityWebRequest.Put(APICall, bytes);
        www.SetRequestHeader("Content-Type", "application/json");
        www.method = "POST";
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError("Network Error for API : " + www.error);
            yield break;
        }

        if (SHOW_DEBUG) Debug.Log("->: " + www.downloadHandler.text);
        //TODO: How to quash this unexpected node type issue??
        awResponse = JsonUtility.FromJson<AWNaturalLanguageResponse>(www.downloadHandler.text);


        callback(awResponse);
    }
    #endregion

    #region Private Methods

    #endregion
}
