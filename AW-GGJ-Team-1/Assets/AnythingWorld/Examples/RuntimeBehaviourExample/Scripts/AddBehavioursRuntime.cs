﻿using System.Collections.Generic;
using UnityEngine;

public class AddBehavioursRuntime : MonoBehaviour
{
    private List<string> scripts;
    private AWObj myCat;
    //int count=0;
    private AnythingCreator anythingCreator;
    private AnythingHabitat anythingHabitat;
    // Start is called before the first frame update
    void Start()
    {
        anythingHabitat = AnythingHabitat.Instance;
        anythingCreator = AnythingCreator.Instance;
        MakeGameActors();
    }

    private void MakeGameActors()
    {

        anythingCreator.MakeObject("camel").AddBehaviour<RandomMovement>();
        AWObj llama = anythingCreator.MakeObject("llama");
        FollowTarget follower = llama.AddBehaviour<FollowTarget>();

        AWObj rat = anythingCreator.MakeObject("rat");
        rat.AddBehaviour<RandomMovement>().SpeedMultiplier = 3f;

        follower.targetController = rat;
    }
}
