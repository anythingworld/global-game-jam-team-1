﻿using UnityEngine;

public class MenuHandler : MonoBehaviour
{
    public GameObject panelObject = null;
    public FlyCamera cameraController = null;
    // Start is called before the first frame update
    void Start()
    {
        panelObject.SetActive(false);
        if (cameraController == null)
        {
            cameraController = Camera.main.gameObject.GetComponent<FlyCamera>();
        }
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("check");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Esc");
            panelObject.SetActive(!panelObject.activeSelf);
            cameraController.TakingInput = !panelObject.activeSelf;
        }
    }
}
