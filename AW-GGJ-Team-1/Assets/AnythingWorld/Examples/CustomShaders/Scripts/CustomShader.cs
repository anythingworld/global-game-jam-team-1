﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class CustomShader : MonoBehaviour
{
    public Material newMaterial = null;
    public void ApplyShader()
    {

        if (newMaterial != null)
        {
            List<MeshRenderer> renderers = (GetComponentsInChildren<MeshRenderer>()).ToList();
            foreach (var renderer in renderers)
            {
                renderer.material = newMaterial;
            }
        }

    }
}
