﻿using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(CustomShader))]
public class CustomShaderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        CustomShader controller = (CustomShader)target;
        base.OnInspectorGUI();
        if (GUILayout.Button("Apply Material"))
        {
            controller.ApplyShader();
        }
    }
}
