﻿using System;

[Serializable]

public class VersionCheck
{
    #region Fields
    public string downloadLink;
    public string version;
    #endregion
}