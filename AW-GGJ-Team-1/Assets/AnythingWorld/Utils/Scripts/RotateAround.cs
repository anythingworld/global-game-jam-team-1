﻿using UnityEngine;

public class RotateAround : MonoBehaviour
{

    public Vector3 RotatePoint;
    public float RotateSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(RotatePoint, transform.up, RotateSpeed);
    }
}
